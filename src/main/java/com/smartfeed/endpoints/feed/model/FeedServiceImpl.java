package com.smartfeed.endpoints.feed.model;

import com.smartfeed.core.exception.ProviderNotFoundException;
import com.smartfeed.endpoints.feed.exception.PostNotFoundException;
import com.smartfeed.social.APIConnector;
import com.smartfeed.social.APILocator;
import com.smartfeed.social.core.feed.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 22:34
 */
@Service
public class FeedServiceImpl implements FeedService {
    @Autowired
    FeedUpdater feedUpdater;
    @Inject
    APILocator apiLocator;
    @Autowired
    @Qualifier("home_feed_repository")
    FeedRepository homeFeedRepository;

    @Autowired
    @Qualifier("user_feed_repository")
    FeedRepository userFeedRepository;

    @Autowired
    @Qualifier("favorites_feed_repository")
    FeedRepository favoritesFeedRepository;

    @Override
    public List<Post> getHomeFeed(FeedPagingParams pagingParams) {
        // если есть post_id, то необходимо получить ленту после данного post_id
        if(pagingParams.getPostId() != null) {
            // получаем пост по post_id
            Post post = homeFeedRepository.get(pagingParams.getPostId());
            // если пост не найден, выкидываем исключение, прерываем выполнение
            if(post == null) {
                throw new PostNotFoundException();
            }

            // если у поста отсутствует информацию о предыдущем посте, обновляем ленту после текущего поста
            if(post.getPrevExternalId() == null) {
                FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(apiLocator.getAPIFor(post.getProviderId()));
                updateOptions.maxPost(post);
                updateOptions.count(pagingParams.getCount());

                return feedUpdater.updateHomeFeed(updateOptions)
                        .getAfter(post.getOriginalTimeCreated(), pagingParams.getCount());
            }

            return homeFeedRepository.getAfter(post.getOriginalTimeCreated(), pagingParams.getCount());
        } else {
            FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(apiLocator.getConnectedAPIs());
            updateOptions.count(pagingParams.getCount());

            return feedUpdater.updateHomeFeed(updateOptions)
                    .getAll(pagingParams.getCount());
        }
    }

    @Override
    public List<Post> getHomeFeed(FeedPagingParams pagingParams, String providerId) {
        APIConnector providerApi = apiLocator.getAPIFor(providerId);
        if(providerApi == null) {
            throw new ProviderNotFoundException();
        }

        if(pagingParams.getPostId() != null) {
            Post post = homeFeedRepository.get(pagingParams.getPostId(), providerId);
            if(post == null) {
                throw new PostNotFoundException();
            }

            if(post.getPrevExternalId() == null) {
                FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(providerApi);
                updateOptions.maxPost(post);
                updateOptions.count(pagingParams.getCount());

                return feedUpdater.updateHomeFeed(updateOptions)
                        .getAfter(post.getOriginalTimeCreated(), pagingParams.getCount(), providerId);
            }

            return homeFeedRepository.getAfter(post.getOriginalTimeCreated(), pagingParams.getCount(), providerId);
        } else {
            FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(providerApi);
            updateOptions.count(pagingParams.getCount());

            return feedUpdater.updateHomeFeed(updateOptions)
                    .getAll(pagingParams.getCount(), providerId);
        }
    }

    @Override
    public List<Post> getUserFeed(FeedPagingParams pagingParams) {
        // если есть post_id, то необходимо получить ленту после данного post_id
        if(pagingParams.getPostId() != null) {
            // получаем пост по post_id
            Post post = userFeedRepository.get(pagingParams.getPostId());
            // если пост не найден, выкидываем исключение, прерываем выполнение
            if(post == null) {
                throw new PostNotFoundException();
            }

            // если у поста отсутствует информацию о предыдущем посте, обновляем ленту после текущего поста
            if(post.getPrevExternalId() == null) {
                FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(apiLocator.getAPIFor(post.getProviderId()));
                updateOptions.maxPost(post);
                updateOptions.count(pagingParams.getCount());

                return feedUpdater.updateUserFeed(updateOptions)
                        .getAfter(post.getOriginalTimeCreated(), pagingParams.getCount());
            }

            return userFeedRepository.getAfter(post.getOriginalTimeCreated(), pagingParams.getCount());
        } else {
            FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(apiLocator.getConnectedAPIs());
            updateOptions.count(pagingParams.getCount());

            return feedUpdater.updateUserFeed(updateOptions)
                    .getAll(pagingParams.getCount());
        }
    }

    @Override
    public List<Post> getUserFeed(FeedPagingParams pagingParams, String providerId) {
        APIConnector providerApi = apiLocator.getAPIFor(providerId);
        if(providerApi == null) {
            throw new ProviderNotFoundException();
        }

        if(pagingParams.getPostId() != null) {
            Post post = userFeedRepository.get(pagingParams.getPostId(), providerId);
            if(post == null) {
                throw new PostNotFoundException();
            }

            if(post.getPrevExternalId() == null) {
                FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(providerApi);
                updateOptions.maxPost(post);
                updateOptions.count(pagingParams.getCount());

                return feedUpdater.updateUserFeed(updateOptions)
                        .getAfter(post.getOriginalTimeCreated(), pagingParams.getCount(), providerId);
            }

            return userFeedRepository.getAfter(post.getOriginalTimeCreated(), pagingParams.getCount(), providerId);
        } else {
            FeedUpdater.UpdateOptions updateOptions = FeedUpdater.UpdateOptions.create(providerApi);
            updateOptions.count(pagingParams.getCount());

            return feedUpdater.updateUserFeed(updateOptions)
                    .getAll(pagingParams.getCount(), providerId);
        }
    }

    @Override
    public List<Post> getFavoritesFeed(FeedPagingParams pagingParams) {
        return null;
    }

    @Override
    public List<Post> getFavoritesFeed(FeedPagingParams pagingParams, String providerId) {
        return null;
    }
}
