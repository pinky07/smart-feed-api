package com.smartfeed.endpoints.feed.model;

import com.smartfeed.social.APIConnector;
import com.smartfeed.social.core.feed.FeedOperations;
import com.smartfeed.social.core.feed.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 20:55
 */
@Service
public class FeedUpdaterImpl implements FeedUpdater {
    @Autowired
    @Qualifier("home_feed_repository")
    FeedRepository homeFeedRepository;

    @Autowired
    @Qualifier("user_feed_repository")
    FeedRepository userFeedRepository;

    @Autowired
    @Qualifier("favorites_feed_repository")
    FeedRepository favoritesFeedRepository;

    @Inject
    ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    public FeedRepository updateHomeFeed(UpdateOptions updateOptions) {
        return updateFeed(createHomeFeedUpdateCallables(updateOptions), homeFeedRepository);
    }

    @Override
    public FeedRepository updateUserFeed(UpdateOptions updateOptions) {
        return updateFeed(createUserFeedUpdateCallables(updateOptions), userFeedRepository);
    }

    @Override
    public FeedRepository updateFavoritesFeed(UpdateOptions updateOptions) {
        return updateFeed(createFavoritesFeedUpdateCallables(updateOptions), favoritesFeedRepository);
    }


    private Collection<Callable<List<Post>>> createHomeFeedUpdateCallables(UpdateOptions updateOptions) {
        Collection<Callable<List<Post>>> callables = new ArrayList<>();
        for(APIConnector connectionAPI : updateOptions.getConnectionAPIs()) {
            if(updateOptions.getPost() != null) {
                callables.add(new HomeFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getPost(), updateOptions.getCount()));
            } else {
                callables.add(new HomeFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getCount()));
            }
        }

        return callables;
    }

    private Collection<Callable<List<Post>>> createUserFeedUpdateCallables(UpdateOptions updateOptions) {
        Collection<Callable<List<Post>>> callables = new ArrayList<>();
        for(APIConnector connectionAPI : updateOptions.getConnectionAPIs()) {
            if(updateOptions.getPost() != null) {
                callables.add(new UserFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getPost(), updateOptions.getCount()));
            } else {
                callables.add(new UserFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getCount()));
            }
        }

        return callables;
    }

    private Collection<Callable<List<Post>>> createFavoritesFeedUpdateCallables(UpdateOptions updateOptions) {
        Collection<Callable<List<Post>>> callables = new ArrayList<>();
        for(APIConnector connectionAPI : updateOptions.getConnectionAPIs()) {
            if(updateOptions.getPost() != null) {
                callables.add(new FavoritesFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getPost(), updateOptions.getCount()));
            } else {
                callables.add(new FavoritesFeedLoaderTask(connectionAPI.feedOperations(), updateOptions.getCount()));
            }
        }

        return callables;
    }


    /**
     * Ассинхронное обновление ленты всех активных соц сетей
     */
    private FeedRepository updateFeed(Collection<Callable<List<Post>>> callables, FeedRepository feedRepository) {
        Collection<Future<List<Post>>> futures = new LinkedList<>();
        for(Callable<List<Post>> callable : callables) {
            futures.add(threadPoolTaskExecutor.submit(callable));
        }

        List<Post> feedList = new ArrayList<>();

        for (Future<List<Post>> future : futures) {
            try {
                feedList.addAll(future.get());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
//            catch (InterruptedException interruptException) {
//                throw new RuntimeException("Update feed error");
//            } catch (ExecutionException exception) {
//                Throwable causedException = exception.getCause();
//                if(causedException instanceof RuntimeException) {
//                    throw (RuntimeException)exception.getCause();
//                } else {
//                    throw new RuntimeException("Update feed error");
//                }
//            }

            feedRepository.saveAll(feedList);
        }

        return feedRepository;

    }


    private static class HomeFeedLoaderTask implements Callable<List<Post>> {
        private FeedOperations feedOperations;
        private Post post;
        private int count;

        private HomeFeedLoaderTask(FeedOperations feedOperations, Post post, int count) {
            this.feedOperations = feedOperations;
            this.post = post;
            this.count = count;
        }

        private HomeFeedLoaderTask(FeedOperations feedOperations, int count) {
            this.feedOperations = feedOperations;
            this.count = count;
        }

        @Override
        public List<Post> call() throws Exception {
            if(post == null) {
                return feedOperations.getHomeFeed(count);
            } else {
                return feedOperations.getHomeFeed(post, count);
            }
        }
    }

    private static class UserFeedLoaderTask implements Callable<List<Post>> {
        private FeedOperations feedOperations;
        private Post post;
        private int count;

        private UserFeedLoaderTask(FeedOperations feedOperations, Post post, int count) {
            this.feedOperations = feedOperations;
            this.post = post;
            this.count = count;
        }

        private UserFeedLoaderTask(FeedOperations feedOperations, int count) {
            this.feedOperations = feedOperations;
            this.count = count;
        }

        @Override
        public List<Post> call() throws Exception {
            if(post == null) {
                return feedOperations.getUserFeed(count);
            } else {
                return feedOperations.getUserFeed(post, count);
            }
        }
    }

    private static class FavoritesFeedLoaderTask implements Callable<List<Post>> {
        private FeedOperations feedOperations;
        private Post post;
        private int count;

        private FavoritesFeedLoaderTask(FeedOperations feedOperations, Post post, int count) {
            this.feedOperations = feedOperations;
            this.post = post;
            this.count = count;
        }

        private FavoritesFeedLoaderTask(FeedOperations feedOperations, int count) {
            this.feedOperations = feedOperations;
            this.count = count;
        }

        @Override
        public List<Post> call() throws Exception {
            if(post == null) {
                return feedOperations.getFavoritesFeed(count);
            } else {
                return feedOperations.getFavoritesFeed(post, count);
            }
        }
    }
}
