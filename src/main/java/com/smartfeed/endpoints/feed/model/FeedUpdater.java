package com.smartfeed.endpoints.feed.model;

import com.smartfeed.social.APIConnector;
import com.smartfeed.social.core.feed.Post;

import java.util.Arrays;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 21:05
 */
public interface FeedUpdater {
    FeedRepository updateHomeFeed(UpdateOptions updateOptions);

    FeedRepository updateUserFeed(UpdateOptions updateOptions);

    FeedRepository updateFavoritesFeed(UpdateOptions updateOptions);


    public static class UpdateOptions {
        private List<APIConnector> connectionAPIs;
        private Post post;
        private int count = 40;

        public static UpdateOptions create(List<APIConnector> connectionAPIs) {
            return new UpdateOptions(connectionAPIs);
        }

        public static UpdateOptions create(APIConnector connectionAPIs) {
            return new UpdateOptions(Arrays.asList(connectionAPIs));
        }

        private UpdateOptions(List<APIConnector> connectionAPIs) {
            this.connectionAPIs = connectionAPIs;
        }

        public UpdateOptions maxPost(Post post) {
            this.post = post;
            return this;
        }

        public UpdateOptions count(int count) {
            this.count = count;
            return this;
        }

        public List<APIConnector> getConnectionAPIs() {
            return connectionAPIs;
        }

        public Post getPost() {
            return post;
        }

        public int getCount() {
            return count;
        }
    }
}
