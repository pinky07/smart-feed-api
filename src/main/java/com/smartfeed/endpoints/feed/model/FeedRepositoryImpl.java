package com.smartfeed.endpoints.feed.model;

import com.smartfeed.endpoints.feed.dao.FeedDao;
import com.smartfeed.social.core.feed.Post;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.11.14
 * Time: 19:04
 */
public class FeedRepositoryImpl implements FeedRepository {
    private String userId;
    private List<String> enabledProviderIds;
    private FeedDao postDao;

    public FeedRepositoryImpl(String userId, List<String> enabledProviderIds, FeedDao postDao) {
        this.userId = userId;
        this.enabledProviderIds = enabledProviderIds;
        this.postDao = postDao;
    }

    private List<Post> excludeRedundant(List<Post> postList) {
        for(Post post : postList) {
            if(post.getPrevExternalId() == null) {
                return postList.subList(0, postList.indexOf(post) + 1);
            }
        }

        return postList;
    }

    @Override
    public List<Post> saveAll(List<Post> postList) {


        return postDao.saveAll(userId, postList);
    }

    @Override
    public List<Post> getAll(int maxCount) {
        return excludeRedundant(postDao.getAll(userId, enabledProviderIds, maxCount));
    }

    @Override
    public Post get(String postId) {
        return postDao.get(userId, enabledProviderIds, postId);
    }

    @Override
    public List<Post> getAfter(Date date, int maxCount) {
        return excludeRedundant(postDao.getAfter(userId, enabledProviderIds, date, maxCount));
    }

    @Override
    public List<Post> getAll(int maxCount, String... providerId) {
        return excludeRedundant(postDao.getAll(userId, Arrays.asList(providerId), maxCount));
    }

    @Override
    public Post get(String postId, String... providerId) {
        return postDao.get(userId, Arrays.asList(providerId), postId);
    }

    @Override
    public List<Post> getAfter(Date date, int maxCount, String... providerId) {
        return excludeRedundant(postDao.getAfter(userId, Arrays.asList(providerId), date, maxCount));
    }
}
