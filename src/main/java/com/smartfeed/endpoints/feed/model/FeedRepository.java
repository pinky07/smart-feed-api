package com.smartfeed.endpoints.feed.model;

import com.smartfeed.social.core.feed.Post;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.11.14
 * Time: 19:03
 */
public interface FeedRepository {
    public List<Post> saveAll(List<Post> postList);

    public List<Post> getAll(int maxCount);

    public List<Post> getAll(int maxCount, String... providerId);

    public Post get(String postId);

    public Post get(String postId, String... providerId);

    public List<Post> getAfter(Date date, int maxCount);

    public List<Post> getAfter(Date date, int maxCount, String... providerId);
}
