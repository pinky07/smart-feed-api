package com.smartfeed.endpoints.feed.model;

import com.smartfeed.social.core.feed.Post;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 22:25
 */
public interface FeedService {
    public List<Post> getHomeFeed(FeedPagingParams pagingParams);
    public List<Post> getHomeFeed(FeedPagingParams pagingParams, String providerId);

    public List<Post> getUserFeed(FeedPagingParams pagingParams);
    public List<Post> getUserFeed(FeedPagingParams pagingParams, String providerId);

    public List<Post> getFavoritesFeed(FeedPagingParams pagingParams);
    public List<Post> getFavoritesFeed(FeedPagingParams pagingParams, String providerId);

    public static class FeedPagingParams {
        private String postId;
        private int count = 40;

        public FeedPagingParams postId(String postId) {
            this.postId = postId;
            return this;
        }

        public FeedPagingParams count(int count) {
            this.count = count;
            return this;
        }

        public String getPostId() {
            return postId;
        }

        public int getCount() {
            return count;
        }
    }
}
