package com.smartfeed.endpoints.feed.resource;

import com.smartfeed.endpoints.feed.model.FeedService;
import com.smartfeed.social.core.feed.Post;
import com.smartfeed.core.resource.BaseResource;
import org.springframework.stereotype.Component;
import com.smartfeed.endpoints.feed.model.FeedService.FeedPagingParams;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 27.10.14
 * Time: 9:29
 */
@Path("/feed")
@Component
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class FeedResource extends BaseResource {
//    @Inject
//    FeedService feedService;

    @Inject
    private FeedService feedService;

    /**
     * Получение агрегированной домашней ленты пользователя
     * @param postId
     * @return
     */
    @GET
    @Path("/home")
    @RolesAllowed({"ROLE_USER"})
    public List<Post> getHomeFeed(@QueryParam("after") String postId) {
        FeedPagingParams feedPagingParams = new FeedPagingParams();
        feedPagingParams.postId(postId);

        List<Post> postList = feedService.getHomeFeed(feedPagingParams);

        return postList;
    }

    @GET
    @Path("/home/{provider_id}")
    @RolesAllowed({"ROLE_USER"})
    public List<Post> getHomeFeed(final @PathParam("provider_id") String providerId, @QueryParam("after") String postId) {
        FeedPagingParams feedPagingParams = new FeedPagingParams();
        feedPagingParams.postId(postId);

        List<Post> postList = feedService.getHomeFeed(feedPagingParams, providerId);

        return postList;
    }

    @GET
    @Path("/user")
    @RolesAllowed({"ROLE_USER"})
    public List<Post> getUserFeed(@QueryParam("after") String postId) {
        FeedPagingParams feedPagingParams = new FeedPagingParams();
        feedPagingParams.postId(postId);

        return feedService.getUserFeed(feedPagingParams);
    }

    @GET
    @Path("/user/{provider_id}")
    @RolesAllowed({"ROLE_USER"})
    public List<Post> getUserFeed(final @PathParam("provider_id") String providerId, @QueryParam("after") String postId) {
        FeedPagingParams feedPagingParams = new FeedPagingParams();
        feedPagingParams.postId(postId);
        feedPagingParams.count(2);

        return feedService.getUserFeed(feedPagingParams, providerId);
    }

    @Path("/favorite")
    public List<Post> getFavoriteFeed() {
        return null;
    }
}
