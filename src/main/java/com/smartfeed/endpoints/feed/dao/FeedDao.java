package com.smartfeed.endpoints.feed.dao;

import com.smartfeed.social.core.feed.Post;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.11.14
 * Time: 18:42
 */
public interface FeedDao {
    public Post save(String userId, Post post);

    public List<Post> saveAll(String userId, List<Post> postList);

    public List<Post> getAll(String userId, int limit);

    public List<Post> getAll(String userId, List<String> providers, int limit);

    public Post get(String userId, String postId);

    public Post get(String userId, List<String> providers, String postId);

    public List<Post> getAfter(String userId, Date date, int limit);

    public List<Post> getAfter(String userId, List<String> providers, Date date, int limit);
}
