package com.smartfeed.endpoints.feed.dao;

import com.smartfeed.endpoints.feed.dao.MongoFeedDao;
import org.springframework.data.mongodb.core.MongoOperations;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 22:06
 */
public class UserFeedDao extends MongoFeedDao {
    private final static String COLLECTION_NAME = "user_feed";

    public UserFeedDao(MongoOperations mongoOperations) {
        super(mongoOperations);
    }

    @Override
    protected String getCollectionName() {
        return COLLECTION_NAME;
    }
}
