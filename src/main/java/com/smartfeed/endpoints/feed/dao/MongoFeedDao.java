package com.smartfeed.endpoints.feed.dao;

import com.smartfeed.endpoints.feed.dao.FeedDao;
import com.smartfeed.social.core.feed.Post;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

/**
 * Author: Khamidullin Kamil
 * Date: 08.11.14
 * Time: 18:44
 */
abstract public class MongoFeedDao implements FeedDao {
    MongoOperations mongoOperations;
    private String collectionName;

    public MongoFeedDao(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
        this.collectionName = getCollectionName();
    }

    abstract protected String getCollectionName();

    @Override
    public Post save(String userId, Post post) {
        Assert.notNull(post, "Post entity must not be null!");
        post.setUserId(userId);
        if(post.getNextExternalId() == null || post.getPrevExternalId() == null) {
            Post postHistory = get(userId, post.getId());
            if(postHistory != null) {
                if(post.getPrevExternalId() == null) {
                    post.getMeta().setPrevExternalId(postHistory.getPrevExternalId());
                }

                if(post.getNextExternalId() == null) {
                    post.getMeta().setNextExternalId(postHistory.getNextExternalId());
                }
            }
        }

        mongoOperations.save(post, collectionName);

        return post;
    }

    @Override
    public List<Post> saveAll(String userId, List<Post> postList) {
        Assert.notNull(postList, "The given Iterable of postList entities not be null!");

        List<Post> result = new ArrayList<Post>();

        for (Post entity : postList) {
            save(userId, entity);
            result.add(entity);
        }

        return result;
    }

    @Override
    public List<Post> getAll(String userId, int limit) {
        Query q = query(where("user_id").is(userId));
        q.with(new Sort(Sort.Direction.DESC, "original_time_created"));
        q.limit(limit);

        return mongoOperations.find(q, Post.class, collectionName);
    }

    @Override
    public List<Post> getAll(String userId, List<String> providers, int limit) {
        Query q = query(where("user_id").is(userId).and("provider_id").in(providers));
        q.with(new Sort(Sort.Direction.DESC, "original_time_created"));
        q.limit(limit);

        return mongoOperations.find(q, Post.class, collectionName);
    }

    @Override
    public Post get(String userId, List<String> providers, String postId) {
        Query q = query(where("user_id").is(userId)
                .and("_id").is(postId)
                .and("provider_id").in(providers));

        return mongoOperations.findOne(q, Post.class, collectionName);
    }

    @Override
    public Post get(String userId, String postId) {
        Query q = query(where("user_id").is(userId).and("_id").is(postId));

        return mongoOperations.findOne(q, Post.class, collectionName);
    }

    @Override
    public List<Post> getAfter(String userId, Date date, int limit) {
        Query q = query(where("user_id").is(userId).and("original_time_created").lt(date));
        q.with(new Sort(Sort.Direction.DESC, "original_time_created"));
        q.limit(limit);

        return mongoOperations.find(q, Post.class, collectionName);
    }

    @Override
    public List<Post> getAfter(String userId, List<String> providers, Date date, int limit) {
        Query q = query(where("user_id").is(userId)
                .and("provider_id").in(providers)
                .and("original_time_created").lt(date));
        q.with(new Sort(Sort.Direction.DESC, "original_time_created"));
        q.limit(limit);

        return mongoOperations.find(q, Post.class, collectionName);
    }
}
