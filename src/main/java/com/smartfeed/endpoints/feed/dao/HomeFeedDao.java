package com.smartfeed.endpoints.feed.dao;

import org.springframework.data.mongodb.core.MongoOperations;

/**
 * Author: Khamidullin Kamil
 * Date: 14.12.14
 * Time: 21:52
 */
public class HomeFeedDao extends MongoFeedDao {
    private final static String COLLECTION_NAME = "home_feed";

    public HomeFeedDao(MongoOperations mongoOperations) {
        super(mongoOperations);
    }

    @Override
    protected String getCollectionName() {
        return COLLECTION_NAME;
    }
}
