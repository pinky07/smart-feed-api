package com.smartfeed.endpoints.feed.exception;

import com.smartfeed.core.exception.BaseWebApplicationException;

/**
 * Author: Khamidullin Kamil
 * Date: 08.11.14
 * Time: 20:31
 */
public class PostNotFoundException extends BaseWebApplicationException {

    public PostNotFoundException() {
        super(404, "Post Not Found", "No Post could be found for that Id");
    }
}