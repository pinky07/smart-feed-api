package com.smartfeed.endpoints.connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 19:53
 */
public class ConnectionStatusServiceImpl implements ConnectionStatusService {
    private ConnectionStatusDao connectionStatusRepository;
    private String userId;

    public ConnectionStatusServiceImpl(ConnectionStatusDao mongoOperations, String userId) {
        this.connectionStatusRepository = mongoOperations;
        this.userId = userId;
    }

    private ConnectionStatus find(String providerId) {
        return connectionStatusRepository.findByUserIdAndProviderId(userId, providerId);
    }

    private ConnectionStatus findOrCreate(String providerId) {
        ConnectionStatus connectionStatus = find(providerId);
        if(connectionStatus == null) {
            connectionStatus = new ConnectionStatus();
            connectionStatus.setUserId(userId);
            connectionStatus.setProviderId(providerId);
        }

        return connectionStatus;
    }

    @Override
    public boolean isEnable(String providerId) {
        ConnectionStatus connectionStatus = find(providerId);
        if(connectionStatus == null) {
            return false;
        }

        return connectionStatus.isEnable();
    }

    @Override
    public void enable(String providerId) {
        setEnable(providerId, true);
    }

    @Override
    public void disable(String providerId) {
        setEnable(providerId, false);
    }

    public List<String> getEnabledProviders() {
        List<ConnectionStatus> connectionStatuses = connectionStatusRepository.findByUserIdAndIsEnable(userId, true);
        List<String> enabledConnections = new ArrayList<>();
        for(ConnectionStatus connectionStatus : connectionStatuses) {
            enabledConnections.add(connectionStatus.getProviderId());
        }

        return enabledConnections;
    }

    private void setEnable(String providerId, boolean value) {
        ConnectionStatus connectionStatus = findOrCreate(providerId);
        connectionStatus.setEnable(value);
        connectionStatusRepository.save(connectionStatus);
    }
}
