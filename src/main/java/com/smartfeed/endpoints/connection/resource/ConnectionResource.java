package com.smartfeed.endpoints.connection.resource;

import com.smartfeed.endpoints.connection.ConnectionService;
import com.smartfeed.endpoints.connection.api.AuthUrlResponse;
import com.smartfeed.endpoints.connection.api.ConnectionResponse;
import com.smartfeed.endpoints.connection.api.CreateConnectionRequest;
import com.smartfeed.endpoints.connection.api.UpdateConnectionRequest;
import com.smartfeed.core.resource.BaseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionData;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 18:37
 */
@Path("/connections")
@Component
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class ConnectionResource extends BaseResource {
    private ConnectionService connectionService;

    @Autowired
    public ConnectionResource(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @POST
    @RolesAllowed({"ROLE_USER"})
    public Response createConnection(CreateConnectionRequest createAccountRequest, @Context UriInfo uriInfo) {
        ConnectionData connectionData = connectionService.createConnection(createAccountRequest);
        URI location = uriInfo.getAbsolutePathBuilder().path(connectionData.getProviderId()).build();
        return Response.created(location).entity(connectionData).build();
    }

    @PUT
    @Path("/{provider_id}")
    @RolesAllowed({"ROLE_USER"})
    public Response updateConnection(final @PathParam("provider_id") String providerId, UpdateConnectionRequest updateAccountRequest) {
        connectionService.updateConnection(providerId, updateAccountRequest);
        return Response.ok().build();
    }

    /**
     * Получение всех социальных сетей пользователя
     * @return
     */
    @GET
    @RolesAllowed({"ROLE_USER"})
    public List<ConnectionResponse> getAllConnections() {
        return connectionService.getAllConnections();
    }

    /**
     * Получение аккацнта социальной сети пользователя по providerId
     * @param providerId
     * @return
     */
    @GET
    @Path("/{provider_id}")
    @RolesAllowed({"ROLE_USER"})
    public ConnectionResponse getConnection(final @PathParam("provider_id") String providerId) {
        return connectionService.getConnectionData(providerId);
    }

    /**
     * Включение социальной сети в агрегированные данные
     * @param providerId
     * @return
     */
    @POST
    @Path("/{provider_id}/enable")
    @RolesAllowed({"ROLE_USER"})
    public Response enableConnection(final @PathParam("provider_id") String providerId) {
        connectionService.enableConnection(providerId);
        return Response.ok().build();
    }

    /**
     * Отключение социальной сети из агрегированных данных
     * @param providerId
     * @return
     */
    @POST
    @Path("/{provider_id}/disable")
    @RolesAllowed({"ROLE_USER"})
    public Response disableConnection(final @PathParam("provider_id") String providerId) {
        connectionService.disableConnection(providerId);
        return Response.ok().build();
    }

    /**
     * Удаление социальной сети из аккаунта пользователя
     * @param providerId
     * @return
     */
    @DELETE
    @Path("/{provider_id}")
    @RolesAllowed({"ROLE_USER"})
    public Response removeConnection(final @PathParam("provider_id") String providerId) {
        connectionService.removeConnection(providerId);

        return Response.ok().build();
    }

    /**
     * Возвращает ссылку для привязки социальной сети к аккаунту пользователя
     * @param providerId
     * @param request
     * @return
     */
    @GET
    @Path("/{provider_id}/auth-url")
    @RolesAllowed({"ROLE_USER"})
    public AuthUrlResponse getAuthUrl(final @PathParam("provider_id") String providerId, @Context HttpServletRequest request) {
        return connectionService.getAuthUrl(providerId, request);
    }
}
