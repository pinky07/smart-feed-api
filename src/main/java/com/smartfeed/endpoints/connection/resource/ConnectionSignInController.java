package com.smartfeed.endpoints.connection.resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import java.lang.reflect.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 20:31
 */
public class ConnectionSignInController extends ProviderSignInController {
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
    private ConnectSupport connectSupport;

    @Value("${hostName.url}")
    private String hostNameUrl;

    final private static String AUTH_COMPLETE_URL = "/v1.0/auth/complete";

    public ConnectionSignInController(ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository usersConnectionRepository, SignInAdapter signInAdapter) {
        super(connectionFactoryLocator, usersConnectionRepository, signInAdapter);
        setSessionStrategy(sessionStrategy);
        setSignUpUrl(AUTH_COMPLETE_URL);
        setSignInUrl(AUTH_COMPLETE_URL);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        connectSupport = new ConnectSupport(sessionStrategy);

        Field privateStringField =
                ProviderSignInController.class.getDeclaredField("connectSupport");
        privateStringField.setAccessible(true);
        privateStringField.set(this, connectSupport);
        privateStringField.setAccessible(false);
    }

    private String buildCallbackUrl(String providerId, NativeWebRequest request) {
        String sessionId = request.getSessionId();
        return hostNameUrl + "/signin/" + providerId + ";jsessionid=" + sessionId;
    }

    @Override
    public RedirectView signIn(@PathVariable String providerId, NativeWebRequest request) {
        connectSupport.setCallbackUrl(buildCallbackUrl(providerId, request));
        return super.signIn(providerId, request);
    }
}
