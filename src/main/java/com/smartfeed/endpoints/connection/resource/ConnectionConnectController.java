package com.smartfeed.endpoints.connection.resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.web.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import java.lang.reflect.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 23:05
 */
public class ConnectionConnectController extends ConnectController {
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
    private ConnectSupport connectSupport;

    @Value("${hostName.url}")
    private String hostNameUrl;

    public ConnectionConnectController(ConnectionFactoryLocator connectionFactoryLocator, ConnectionRepository connectionRepository) {
        super(connectionFactoryLocator, connectionRepository);
    }

    private String createVersionedPath(String resource) {
        return hostNameUrl + "/v1.0/" + resource;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        connectSupport = new ConnectSupport(sessionStrategy);

        Field privateStringField =
                ConnectController.class.getDeclaredField("connectSupport");
        privateStringField.setAccessible(true);
        privateStringField.set(this, connectSupport);
        privateStringField.setAccessible(false);
    }

    private String buildCallbackUrl(String providerId, NativeWebRequest request) {
        String sessionId = request.getSessionId();

        return hostNameUrl + "/connect/" + providerId + ";jsessionid=" + sessionId;
    }

    @Override
    public RedirectView connect(@PathVariable String providerId, NativeWebRequest request) {
        connectSupport.setCallbackUrl(buildCallbackUrl(providerId, request));
        return super.connect(providerId, request);
    }

    @Override
    protected RedirectView connectionStatusRedirect(String providerId, NativeWebRequest request) {
        String path = "/connection/" + providerId + "/complete";

        return new RedirectView(path, true);
    }
}
