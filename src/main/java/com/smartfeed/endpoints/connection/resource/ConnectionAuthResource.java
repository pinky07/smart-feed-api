package com.smartfeed.endpoints.connection.resource;

import com.smartfeed.core.api.ErrorResponse;
import com.smartfeed.endpoints.connection.ConnectionStatusService;
import com.smartfeed.endpoints.connection.api.AuthErrorResponse;
import com.smartfeed.endpoints.connection.api.AuthResponse;
import com.smartfeed.endpoints.connection.exception.AuthException;
import com.smartfeed.endpoints.connection.exception.InvalidSessionException;
import com.smartfeed.core.exception.BaseWebApplicationException;
import com.smartfeed.endpoints.signin.SignInUtils;
import com.smartfeed.endpoints.user.api.ApiUser;
import com.smartfeed.endpoints.user.model.User;
import com.smartfeed.endpoints.user.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 21:00
 */
@Controller
@RequestMapping("/v1.0")
public class ConnectionAuthResource {
    @Autowired
    DefaultTokenServices tokenServices;
    @Autowired
    ClientDetailsService clientDetailsService;

    @Autowired
    UserService userService;

    @Autowired
    ProviderSignInController signInController;

    @Autowired
    ConnectController connectController;

    @Autowired
    ConnectionStatusService connectionStatusService;

    private ProviderSignInUtils providerSignInUtils;

    private final static Log logger = LogFactory.getLog(ConnectionAuthResource.class);

    public ConnectionAuthResource() {
        this.providerSignInUtils = new ProviderSignInUtils();
    }

    @ExceptionHandler(Exception.class)
    //@ResponseBody
    public ResponseEntity<OAuth2Exception> handleError(HttpServletRequest req, BaseWebApplicationException exception) {
        //return new AuthErrorResponse(exception.getErrorResponse());
        return new ResponseEntity<>(OAuth2Exception.create("connection_auth_error", exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Авторизация через соц сеть
     * @param providerId
     * @param nativeRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/auth/{providerId}", method=RequestMethod.GET)
    public RedirectView auth(@PathVariable String providerId, NativeWebRequest nativeRequest)
            throws Exception {
        return signInController.signIn(providerId, nativeRequest);
    }

    /**
     * Добавление соц сети к текущему аккаунту
     * @param providerId
     * @param nativeRequest
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/connect/{providerId}", method=RequestMethod.GET)
    public RedirectView addConnection(@PathVariable String providerId, NativeWebRequest nativeRequest)
            throws Exception {

        HttpServletRequest request =
                (HttpServletRequest) nativeRequest.getNativeRequest();

        if(request.isRequestedSessionIdFromURL() && !request.isRequestedSessionIdValid()) {
            throw new InvalidSessionException();
        }

        return connectController.connect(providerId, nativeRequest);
    }

    @ResponseBody
    @RequestMapping(value="/auth/complete", method= RequestMethod.GET)
    public OAuth2AccessToken authSuccess(NativeWebRequest request, @RequestParam(value="error", required=false) String error) {
        if(error != null) {
            if(error.equals("multiple_users")) {
                throw new AuthException("ERROR_MULTIPLE_USERS", "This account has some users");
            } else  {
                throw new AuthException();
            }
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!isAuthenticated(authentication)) {
            Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
            if(connection == null) {
                throw new AuthException();
            }

            User user = userService.createUser(connection);
            providerSignInUtils.doPostSignUp(user.getId(), request);
            authentication = SignInUtils.createAuthenticationForUser(user);
        }

        //User user = (User)authentication.getPrincipal();

        return createOAuth2AccessToken(authentication);
    }

    @RequestMapping(value = "/connect/{providerId}/complete", method= RequestMethod.GET)
    @ResponseBody
    public String connectAdded(@PathVariable String providerId) {
        connectionStatusService.enable(providerId);

        return "connection " + providerId + " added";
    }

    private OAuth2AccessToken createOAuth2AccessToken(Authentication authentication) {
        ClientDetails clientDetails = clientDetailsService.loadClientByClientId("353b302c44574f565045687e534e7d6a");
        OAuth2Request oAuth2Request = new OAuth2Request(new HashMap<>(), clientDetails.getClientId(),
                null, true, clientDetails.getScope(), null, null, null, null
        );

        OAuth2Authentication auth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);

        return tokenServices.createAccessToken(auth2Authentication);
    }

    private boolean isAuthenticated(Authentication authentication) {
        return !(authentication instanceof AnonymousAuthenticationToken);
    }
}
