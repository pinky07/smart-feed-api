package com.smartfeed.endpoints.connection;

import com.smartfeed.endpoints.connection.api.AuthUrlResponse;
import com.smartfeed.endpoints.connection.api.ConnectionResponse;
import com.smartfeed.endpoints.connection.api.CreateConnectionRequest;
import com.smartfeed.endpoints.connection.api.UpdateConnectionRequest;
import com.smartfeed.endpoints.connection.exception.ConnectionNotFoundException;
import com.smartfeed.endpoints.connection.exception.DuplicateAccountException;
import com.smartfeed.core.service.BaseService;
import com.smartfeed.endpoints.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.*;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 20:10
 */
@Service
public class ConnectionServiceImpl extends BaseService implements ConnectionService {

    private Logger LOG = LoggerFactory.getLogger(UserService.class);
    ConnectionRepository connectionRepository;
    ConnectionFactoryLocator connectionFactoryLocator;
    ConnectionStatusService connectionStatusService;

    @Value("${hostName.url}")
    private String hostNameUrl;

    @Autowired
    public ConnectionServiceImpl(ConnectionRepository connectionRepository, ConnectionFactoryLocator connectionFactoryLocator, ConnectionStatusService connectionStatusService, Validator validator) {
        super(validator);
        this.connectionRepository = connectionRepository;
        this.connectionFactoryLocator = connectionFactoryLocator;
        this.connectionStatusService = connectionStatusService;
    }

    public ConnectionData createConnection(CreateConnectionRequest createAccountRequest) {
        LOG.info("Validating create account request.");
        validate(createAccountRequest);

        final String providerId = createAccountRequest.getProviderId();
        List<Connection<?>> connections = connectionRepository.findConnections(providerId);

        if (connections.size() == 0) {
            LOG.info("Connection does not already exist in the data store - creating a new account [{}].",
                    providerId);

            ConnectionFactory connectionFactory = connectionFactoryLocator.getConnectionFactory(providerId);
            Connection<?> connection = createConnection(connectionFactory, new CreateConnectionRequestConverter(createAccountRequest));
            connectionRepository.addConnection(connection);

            LOG.debug("Created new connection [{}].", providerId);

            enableConnection(providerId);

            LOG.debug("Enabled connection [{}].", providerId);

            return connection.createData();

        } else {
            LOG.info("Duplicate connection located, exception raised with appropriate HTTP response code.");
            throw new DuplicateAccountException();
        }
    }

    private Connection<?> createConnection(ConnectionFactory connectionFactory, CreateConnectionRequestConverter createConnectionRequestConverter) {
        Connection<?> connection;
        if(connectionFactory instanceof OAuth2ConnectionFactory) {
            connection = ((OAuth2ConnectionFactory<?>)connectionFactory).createConnection(createConnectionRequestConverter.accessGrant());
        } else {
            connection = ((OAuth1ConnectionFactory<?>)connectionFactory).createConnection(createConnectionRequestConverter.oAuthToken());
        }

        return connection;
    }

    public ConnectionData updateConnection(String providerId, UpdateConnectionRequest updateAccountRequest) {
        Assert.notNull(providerId);
        Assert.notNull(updateAccountRequest);
        LOG.info("Validating update account request.");
        validate(updateAccountRequest);
        Connection<?> connection = getConnection(providerId);
        if(connection == null) {
            throw new ConnectionNotFoundException();
        }
        ConnectionData connectionData = connection.createData();
        ConnectionFactory<?> connectionFactory =  connectionFactoryLocator.getConnectionFactory(providerId);
        Connection<?> updatedConnection = connectionFactory.createConnection(new ConnectionData(
                providerId,
                connectionData.getProviderUserId(),
                connectionData.getDisplayName(),
                connectionData.getProfileUrl(),
                connectionData.getImageUrl(),
                updateAccountRequest.getAccessToken(),
                updateAccountRequest.getAccessTokenSecret(),
                updateAccountRequest.getRefreshToken(),
                updateAccountRequest.getExpireTime()
        ));

        connectionRepository.updateConnection(updatedConnection);

        LOG.debug("Updated connection [{}].", providerId);

        return updatedConnection.createData();
    }

    private Connection<?> getConnection(String providerId) {
        List<Connection<?>> connections = connectionRepository.findConnections(providerId);
        if(connections.isEmpty()) {
            return null;
        }

        return connections.get(0);
    }

    public ConnectionResponse getConnectionData(String providerId) {
        Assert.notNull(providerId);
        Connection<?> connection = getConnection(providerId);
        if(connection == null) {
            throw new ConnectionNotFoundException();
        }

        ConnectionResponse connectionResponse = new ConnectionResponse(connection.createData());
        connectionResponse.setEnabled(connectionStatusService.isEnable(providerId));

        return connectionResponse;
    }

    public List<ConnectionResponse> getAllConnections() {
        List<ConnectionResponse> connections = new ArrayList<>();
        Collection<List<Connection<?>>> batchConnections = connectionRepository.findAllConnections().values();
        for(List<Connection<?>> connectionList : batchConnections) {
            for (Connection<?> connection : connectionList) {
                ConnectionResponse connectionResponse = new ConnectionResponse(connection.createData());
                connectionResponse.setEnabled(connectionStatusService.isEnable(connectionResponse.getProviderId()));
                connections.add(connectionResponse);
            }
        }

        return connections;
    }

    public void enableConnection(String providerId) {
        connectionStatusService.enable(providerId);
    }

    public void disableConnection(String providerId) {
        connectionStatusService.disable(providerId);
    }

    public void removeConnection(String providerId) {
        connectionRepository.removeConnections(providerId);
    }

    @Override
    public AuthUrlResponse getAuthUrl(String providerId, HttpServletRequest request) {
        String sessionId = request.getSession().getId();

        String url = hostNameUrl + "/v1.0/connect/" + providerId + ";jsessionid=" + sessionId;

        return new AuthUrlResponse(url);
    }
}
