package com.smartfeed.endpoints.connection;

import com.smartfeed.core.persistence.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 19:56
 */
@Document(collection = "connection_status")
public class ConnectionStatus extends BaseEntity {
    @Field("user_id")
    private String userId;
    @Field("provider_id")
    private String providerId;
    @Field("is_enable")
    private boolean isEnable;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }
}
