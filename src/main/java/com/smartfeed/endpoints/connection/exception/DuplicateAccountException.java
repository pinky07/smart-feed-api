package com.smartfeed.endpoints.connection.exception;

import com.smartfeed.core.exception.BaseWebApplicationException;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 22:02
 */
public class DuplicateAccountException extends BaseWebApplicationException {
    public DuplicateAccountException() {
        super(409, "ERROR_DUPLICATE_ACCOUNT","Connection already exists", "An attempt was made to create a connection that already exists");
    }
}
