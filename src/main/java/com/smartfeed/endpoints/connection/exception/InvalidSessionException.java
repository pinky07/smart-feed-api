package com.smartfeed.endpoints.connection.exception;

import com.smartfeed.core.exception.BaseWebApplicationException;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 23:09
 */
public class InvalidSessionException extends BaseWebApplicationException {
    public InvalidSessionException() {
        super(500, "ERROR_INVALID_SESSION", "Invalid session", "Invalid session");
    }
}
