package com.smartfeed.endpoints.connection.exception;

import com.smartfeed.core.exception.BaseWebApplicationException;

/**
 * Author: Khamidullin Kamil
 * Date: 21.12.14
 * Time: 1:23
 */
public class AuthException extends BaseWebApplicationException {
    public AuthException(String code, String message) {
        super(500, code, "Authenticate error", message);
    }

    public AuthException() {
        super(500, "ERROR_UNKNOWN", "Authenticate error", "Unknown authenticate error");
    }
}
