package com.smartfeed.endpoints.connection.exception;

import com.smartfeed.core.exception.BaseWebApplicationException;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 22:19
 */
public class ConnectionNotFoundException extends BaseWebApplicationException {

    public ConnectionNotFoundException() {
        super(404, "ERROR_CONNECTION_NOT_FOUND","Connection Not Found", "No Connection could be found for that social network code");
    }
}