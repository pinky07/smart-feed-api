package com.smartfeed.endpoints.connection;

import com.smartfeed.endpoints.connection.api.AuthUrlResponse;
import com.smartfeed.endpoints.connection.api.ConnectionResponse;
import com.smartfeed.endpoints.connection.api.CreateConnectionRequest;
import com.smartfeed.endpoints.connection.api.UpdateConnectionRequest;
import org.springframework.social.connect.ConnectionData;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 20:10
 */
public interface ConnectionService {
    public ConnectionData createConnection(CreateConnectionRequest createAccountRequest);

    public ConnectionData updateConnection(String providerId, UpdateConnectionRequest updateAccountRequest);

    public ConnectionResponse getConnectionData(String providerId);

    public List<ConnectionResponse> getAllConnections();

    public void enableConnection(String providerId);

    public void disableConnection(String providerId);

    public void removeConnection(String providerId);

    public AuthUrlResponse getAuthUrl(String providerId, HttpServletRequest request);
}
