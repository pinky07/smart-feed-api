package com.smartfeed.endpoints.connection.api;

import javax.validation.constraints.NotNull;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 20:03
 */
public class UpdateConnectionRequest {
    @NotNull
    private String accessToken;
    private String accessTokenSecret;
    private String refreshToken;
    private String scope;
    private Long expireTime;

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getScope() {
        return scope;
    }

    public Long getExpireTime() {
        return expireTime;
    }
}
