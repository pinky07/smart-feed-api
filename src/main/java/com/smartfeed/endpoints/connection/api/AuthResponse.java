package com.smartfeed.endpoints.connection.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartfeed.endpoints.user.api.ApiUser;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * Author: Khamidullin Kamil
 * Date: 21.12.14
 * Time: 22:34
 */
public class AuthResponse {
    private ApiUser apiUser;
    private OAuth2AccessToken oAuth2AccessToken;

    public AuthResponse(ApiUser apiUser, OAuth2AccessToken oAuth2AccessToken) {
        this.apiUser = apiUser;
        this.oAuth2AccessToken = oAuth2AccessToken;
    }

    @JsonProperty(value = "api_user")
    public ApiUser getApiUser() {
        return apiUser;
    }

    @JsonProperty(value = "oauth2_access_token")
    public OAuth2AccessToken getoAuth2AccessToken() {
        return oAuth2AccessToken;
    }
}
