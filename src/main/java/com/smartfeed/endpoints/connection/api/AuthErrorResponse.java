package com.smartfeed.endpoints.connection.api;

import com.smartfeed.core.api.ErrorResponse;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 21:37
 */
public class AuthErrorResponse {
    private String errorCode;
    private String consumerMessage;
    private String applicationMessage;

    public AuthErrorResponse(ErrorResponse errorResponse) {
        this.errorCode = errorResponse.getError();
        this.consumerMessage = errorResponse.getErrorDescription();
        this.applicationMessage = errorResponse.getApplicationMessage();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getConsumerMessage() {
        return consumerMessage;
    }

    public String getApplicationMessage() {
        return applicationMessage;
    }
}
