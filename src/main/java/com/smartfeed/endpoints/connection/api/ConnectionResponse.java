package com.smartfeed.endpoints.connection.api;

import org.springframework.social.connect.ConnectionData;

/**
 * Author: Khamidullin Kamil
 * Date: 15.02.15
 * Time: 12:26
 */
public class ConnectionResponse {
    private String providerId;

    private String providerUserId;

    private String displayName;

    private String profileUrl;

    private String imageUrl;

    private boolean enabled;

    public ConnectionResponse(ConnectionData connectionData) {
        this.providerId = connectionData.getProviderId();
        this.providerUserId = connectionData.getProviderUserId();
        this.displayName = connectionData.getDisplayName();
        this.profileUrl = connectionData.getProfileUrl();
        this.imageUrl = connectionData.getImageUrl();
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getProviderId() {
        return providerId;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
