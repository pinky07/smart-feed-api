package com.smartfeed.endpoints.connection.api;

import javax.validation.constraints.NotNull;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 20:18
 */
public class CreateConnectionRequest {
    @NotNull
    private String providerId;
    @NotNull
    private String accessToken;
    private String accessTokenSecret;
    private String refreshToken;
    private String scope;
    private Long expireTime;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
