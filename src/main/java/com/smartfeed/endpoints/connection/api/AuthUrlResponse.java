package com.smartfeed.endpoints.connection.api;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 30.11.14
 * Time: 15:22
 */
public class AuthUrlResponse implements Serializable {
    private String url;

    public AuthUrlResponse(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
