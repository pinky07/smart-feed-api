package com.smartfeed.endpoints.connection;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 07.12.14
 * Time: 21:35
 */
public interface ConnectionStatusDao extends MongoRepository<ConnectionStatus, String> {
    ConnectionStatus findByUserIdAndProviderId(String userId, String providerId);
    List<ConnectionStatus> findByUserIdAndIsEnable(String userId, boolean status);
}
