package com.smartfeed.endpoints.connection;

import com.smartfeed.endpoints.connection.api.CreateConnectionRequest;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.oauth2.AccessGrant;

/**
 * Author: Khamidullin Kamil
 * Date: 03.11.14
 * Time: 16:20
 */
public class CreateConnectionRequestConverter {
    private CreateConnectionRequest createAccountRequest;

    public CreateConnectionRequestConverter(CreateConnectionRequest createAccountRequest) {
        this.createAccountRequest = createAccountRequest;
    }

    public AccessGrant accessGrant() {
        AccessGrant connectionData = new AccessGrant(
            createAccountRequest.getAccessToken(),
            createAccountRequest.getScope(),
            createAccountRequest.getRefreshToken(),
            createAccountRequest.getExpireTime()
        );

        return connectionData;
    }

    public OAuthToken oAuthToken() {
        OAuthToken oAuthToken = new OAuthToken(
            createAccountRequest.getAccessToken(),
            createAccountRequest.getAccessTokenSecret()
        );

        return oAuthToken;
    }
}
