package com.smartfeed.endpoints.connection;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 19:51
 */
public interface ConnectionStatusService {
    public boolean isEnable(String providerId);
    public void enable(String providerId);
    public void disable(String providerId);
    public List<String> getEnabledProviders();
}
