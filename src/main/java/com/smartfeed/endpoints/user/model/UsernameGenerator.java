package com.smartfeed.endpoints.user.model;

import com.smartfeed.endpoints.user.api.CreateUserRequest;
import org.springframework.social.connect.UserProfile;

/**
 * Author: Khamidullin Kamil
 * Date: 18.12.14
 * Time: 19:00
 */
public class UsernameGenerator {
    private UserRepository userRepository;

    public UsernameGenerator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private boolean isUserNameAvailable(String userName) {
        return !userRepository.exists(userName);
    }

    /**
     * Получение логина из email'a
     * @param email
     * @return
     */
    private String userNameFromEmail(String email) {
        String[] temp;
        String delimiter = "@";
        temp = email.split(delimiter);
        if(temp.length > 1) {
            return temp[0];
        }

        return null;
    }

    /**
     *
     * @param usernameTmp
     * @return
     */
    private String generateUsernameWithPostfix(String usernameTmp) {
        if(usernameTmp == null) {
            usernameTmp = "user";
        }

        int usernamePostfix = 1;
        String username = usernameTmp + String.valueOf(usernamePostfix);

        while (!isUserNameAvailable(username)) {
            usernamePostfix++;
            username = usernameTmp + String.valueOf(usernamePostfix);
        }

        return username;
    }

    /**
     *
     * @param createUserRequest
     * @return
     */
    public String generateUsername(CreateUserRequest createUserRequest) {
        String tempUsername = userNameFromEmail(createUserRequest.getUser().getEmailAddress());
        if(tempUsername != null && isUserNameAvailable(tempUsername)) {
            return tempUsername;
        } else {
            return generateUsernameWithPostfix(tempUsername);
        }
    }

    /**
     *
     * @param userProfile
     * @return
     */
    public String generateUsername(UserProfile userProfile) {
        String connectionUserName = userProfile.getUsername();
        if(connectionUserName != null && isUserNameAvailable(connectionUserName)) {
            return connectionUserName;
        }

        String connectionEmail = userProfile.getEmail();
        if(connectionEmail != null) {
            String connectionEmailUsername = userNameFromEmail(connectionEmail);
            if(connectionEmailUsername != null && isUserNameAvailable(connectionEmailUsername)) {
                return connectionEmailUsername;
            }
        }

        String usernameTmp = (connectionUserName != null)
                ? connectionUserName : connectionEmail;

        return generateUsernameWithPostfix(usernameTmp);
    }


}
