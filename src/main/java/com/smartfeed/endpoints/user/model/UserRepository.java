package com.smartfeed.endpoints.user.model;

/**
 * Author: Khamidullin Kamil
 * Date: 18.12.14
 * Time: 15:08
 */
public interface UserRepository {
    public User findByUserName(final String username);

    public User findByEmailAddress(final String email);

    public User findById(final String id);

    public User save(User user);

    public boolean exists(String username);
}
