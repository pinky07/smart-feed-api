package com.smartfeed.endpoints.user.model;

import com.smartfeed.endpoints.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Author: Khamidullin Kamil
 * Date: 18.12.14
 * Time: 14:41
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
    private UserDao userDao;

    @Autowired
    public UserRepositoryImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User findByUserName(String userName) {
        return userDao.findByUserName(userName);
    }

    @Override
    public User findByEmailAddress(String email) {
        return userDao.findByEmailAddress(email);
    }

    @Override
    public User findById(String id) {
        return userDao.findById(id);
    }

    @Override
    public User save(User user) {
        return userDao.save(user);
    }

    @Override
    public boolean exists(String username) {
        return findByUserName(username) != null;
    }
}
