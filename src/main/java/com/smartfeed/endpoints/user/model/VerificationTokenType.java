package com.smartfeed.endpoints.user.model;

/**
 * @author: Iain Porter
 */
public enum VerificationTokenType {

    lostPassword, emailVerification, emailRegistration
}
