package com.smartfeed.endpoints.user.model;

public enum Role {
    
    ROLE_USER, ROLE_ADMIN
}
