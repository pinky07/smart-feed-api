package com.smartfeed.endpoints.user.model;

import com.smartfeed.core.persistence.BaseEntity;
import com.smartfeed.endpoints.user.api.ApiUser;
import com.mongodb.DBObject;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 24/04/2013
 */
@Document(collection = "user")
public class User extends BaseEntity implements UserDetails {
    @Field("user_name")
    private String userName;
    @Field("email_address")
    private String emailAddress;
    @Field("first_name")
    private String firstName;
    @Field("last_name")
    private String lastName;
    private Integer age;
    @Field("hashed_password")
    private String hashedPassword;
    private Boolean verified = false;
    private List<Role> roles = new ArrayList<Role>();

    public User() {
        super();
        this.addRole(Role.ROLE_USER);
    }

    public User(String id) {
        super(id);
        this.addRole(Role.ROLE_USER);
    }

    public User(final ApiUser apiUser) {
        this();
        this.userName = apiUser.getUsername();
        this.emailAddress = apiUser.getEmailAddress().toLowerCase();
        this.firstName = apiUser.getFirstName();
        this.lastName = apiUser.getLastName();
        this.age = apiUser.getAge();
    }

    @SuppressWarnings("unchecked")
    public User(DBObject dbObject) {
        this((String)dbObject.get("_id"));
        this.userName = (String)dbObject.get("user_name");
        this.emailAddress = (String)dbObject.get("email_address");
        this.firstName = (String)dbObject.get("first_name");
        this.lastName = (String)dbObject.get("last_name");
        this.hashedPassword = (String)dbObject.get("hashed_password");
        this.verified = (Boolean)dbObject.get("verified");
        List<String> roles = (List<String>)dbObject.get("roles");
        deSerializeRoles(roles);
    }

    private void deSerializeRoles(List<String> roles) {
         for(String role : roles) {
             this.addRole(Role.valueOf(role));
         }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for( Role role : this.getRoles() ) {
            GrantedAuthority authority = new SimpleGrantedAuthority(role.name());
            authorities.add(authority);
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return hashedPassword;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean isVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public List<Role> getRoles() {
        return Collections.unmodifiableList(this.roles);
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public boolean hasRole(Role role) {
        return (this.roles.contains(role));
    }
}