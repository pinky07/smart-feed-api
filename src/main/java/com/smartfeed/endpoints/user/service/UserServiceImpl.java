package com.smartfeed.endpoints.user.service;

import com.smartfeed.core.service.BaseService;
import com.smartfeed.endpoints.user.api.ApiUser;
import com.smartfeed.endpoints.user.model.Role;
import com.smartfeed.endpoints.user.model.User;
import com.smartfeed.endpoints.user.dao.UserDao;
import com.smartfeed.endpoints.user.api.CreateUserRequest;
import com.smartfeed.endpoints.user.api.UpdateUserRequest;
import com.smartfeed.endpoints.user.exception.AuthenticationException;
import com.smartfeed.endpoints.user.exception.DuplicateUserException;
import com.smartfeed.endpoints.user.exception.UserNotFoundException;
import com.smartfeed.endpoints.user.model.UserRepository;
import com.smartfeed.endpoints.user.model.UsernameGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.validation.Validator;

import static org.springframework.util.Assert.notNull;



@Service
public class UserServiceImpl extends BaseService implements UserService, UserDetailsService {

    private Logger LOG = LoggerFactory.getLogger(UserService.class);
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UsernameGenerator usernameGenerator;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository, Validator validator,
                           PasswordEncoder passwordEncoder) {
        super(validator);
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.usernameGenerator = new UsernameGenerator(userRepository);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return locateUser(username);
    }

    @Transactional
    public User createUser(final CreateUserRequest createUserRequest) {
        LOG.info("Validating user request.");
        validate(createUserRequest);
        final String emailAddress = createUserRequest.getUser().getEmailAddress().toLowerCase();
        if (userRepository.findByEmailAddress(emailAddress) == null) {
            LOG.info("User does not already exist in the data store - creating a new user [{}].", emailAddress);
            ApiUser createUserData = createUserRequest.getUser();
            String username = createUserData.getUsername();
            if(username != null && userRepository.findByUserName(createUserData.getUsername()) != null) {
                LOG.info("Duplicate user located, exception raised with appropriate HTTP response code.");
                throw new DuplicateUserException();
            }

            User user = new User(createUserData);
            user.setUserName((username != null) ? username : usernameGenerator.generateUsername(createUserRequest));
            user.setHashedPassword(passwordEncoder.encode(createUserRequest.getPassword().getPassword()));

            userRepository.save(user);

            LOG.debug("Created new user [{}].", user.getUsername());

            return user;
        } else {
            LOG.info("Duplicate user located, exception raised with appropriate HTTP response code.");
            throw new DuplicateUserException();
        }
    }

    @Override
    public User createUser(Connection connection) {
        UserProfile userProfile = connection.fetchUserProfile();
        User user = new User();
        user.setFirstName(userProfile.getFirstName());
        user.setLastName(userProfile.getLastName());
        user.setUserName(usernameGenerator.generateUsername(userProfile));
        user.setEmailAddress(userProfile.getEmail());

        userRepository.save(user);

        LOG.debug("Created new user [{}].", user.getUsername());

        return user;
    }

    @Override
    public User authenticate(String username, String password) {
        Assert.notNull(username);
        Assert.notNull(password);
        User user = locateUser(username);
        if(!passwordEncoder.encode(password).equals(user.getHashedPassword())) {
            throw new AuthenticationException();
        }
        return user;
    }

    @Transactional
    public User saveUser(String userId, UpdateUserRequest request) {
        validate(request);
        User user = userRepository.findById(userId);
        if(user == null) {
            throw new UserNotFoundException();
        }
        if(request.getFirstName() != null) {
            user.setFirstName(request.getFirstName());
        }
        if(request.getLastName() != null) {
            user.setLastName(request.getLastName());
        }
        if(request.getEmailAddress() != null) {
            if(!request.getEmailAddress().equals(user.getEmailAddress())) {
                user.setEmailAddress(request.getEmailAddress());
                user.setVerified(false);
            }
        }
        userRepository.save(user);
        return user;
    }

    @Override
    public User getUser(String userId) {
        Assert.notNull(userId);
        User user = userRepository.findById(userId);
        if(user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    /**
     * Locate the user and throw an exception if not found.
     *
     * @param username
     * @return a User object is guaranteed.
     * @throws AuthenticationException if user not located.
     */
    private User locateUser(final String username) {
        notNull(username, "Mandatory argument 'username' missing.");
        User user = userRepository.findByUserName(username.toLowerCase());
        if (user == null) {
            LOG.debug("Credentials [{}] failed to locate a user - hint, username.", username.toLowerCase());
            throw new AuthenticationException();
        }
        return user;
    }
}