package com.smartfeed.endpoints.user.service;

import com.smartfeed.endpoints.user.model.User;
import com.smartfeed.endpoints.user.api.CreateUserRequest;
import com.smartfeed.endpoints.user.api.UpdateUserRequest;
import org.springframework.social.connect.Connection;

public interface UserService {

    public User createUser(final CreateUserRequest createUserRequest);

    public User createUser(final Connection connection);

    public User authenticate(String username, String password);

    public User getUser(String userId);

    /**
     * Save User
     *
     * @param userId
     * @param request
     */
    public User saveUser(String userId, UpdateUserRequest request);
}
