package com.smartfeed.endpoints.user.dao;

import com.smartfeed.endpoints.user.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserDao extends MongoRepository<User, String> {

    public User findByUserName(String userName);

    public User findByEmailAddress(final String name);

    public User findById(final String id);
}
