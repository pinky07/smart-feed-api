package com.smartfeed.endpoints.user.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartfeed.endpoints.user.model.User;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.security.Principal;

import static org.springframework.util.Assert.notNull;

@XmlRootElement
public class ApiUser implements Principal {

    @NotNull
    private String emailAddress;
    @Length(max = 80)
    private String username;
    @Length(max = 80)
    private String firstName;
    @Length(max = 80)
    private String lastName;
    private Integer age;
    private String id;

    public ApiUser() {
    }

    public ApiUser(User user) {
        this.emailAddress = user.getEmailAddress();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.id = user.getId();
        this.age = user.getAge();
    }

    @JsonProperty(value = "email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        notNull(emailAddress, "Mandatory argument 'emailAddress missing.'");
        this.emailAddress = emailAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        notNull(id, "Mandatory argument 'id missing.'");
        this.id = id;
    }

    @JsonProperty(value = "first_name")
    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(final String firstName) {
        notNull(firstName, "Mandatory argument 'firstName missing.'");
        this.firstName = firstName;
    }

    @JsonProperty(value = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        notNull(lastName, "Mandatory argument 'lastName missing.'");
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getName() {
        return getId();
    }
}