package com.smartfeed.endpoints.oauth2;

import com.smartfeed.core.persistence.BaseEntity;
import com.smartfeed.endpoints.user.model.User;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 22/05/2013
 */
@Document(collection = "access_token")
public class OAuth2AuthenticationAccessToken extends BaseEntity {
    public static String USERNAME_FIELD = "username";
    public static String USER_ID_FIELD = "user_id";

    @Field("token_id")
    private String tokenId;
    @Field("access_token")
    private OAuth2AccessToken oAuth2AccessToken;
    @Field("authentication_id")
    private String authenticationId;
    @Field("user_name")
    private String userName;
    @Field("client_id")
    private String clientId;
    private OAuth2Authentication authentication;
    @Field("refresh_token")
    private String refreshToken;

    public OAuth2AuthenticationAccessToken() {
    }

    public OAuth2AuthenticationAccessToken(final OAuth2AccessToken oAuth2AccessToken, final OAuth2Authentication authentication, final String authenticationId) {
        if(oAuth2AccessToken.getAdditionalInformation().isEmpty() &&
                oAuth2AccessToken instanceof DefaultOAuth2AccessToken) {
            if(authentication.getPrincipal() instanceof User) {
                User user = (User)authentication.getPrincipal();
                Map<String, Object> additionalInformation = new HashMap<>();
                additionalInformation.put(USER_ID_FIELD, user.getId());
                additionalInformation.put(USERNAME_FIELD, user.getUsername());
                ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionalInformation);
            }
        }

        this.tokenId = oAuth2AccessToken.getValue();
        this.oAuth2AccessToken = oAuth2AccessToken;
        this.authenticationId = authenticationId;
        this.userName = authentication.getName();
        this.clientId = authentication.getOAuth2Request().getClientId();
        this.authentication = authentication;
        this.refreshToken = oAuth2AccessToken.getRefreshToken().getValue();
    }

    public String getTokenId() {
        return tokenId;
    }

    public OAuth2AccessToken getoAuth2AccessToken() {
        return oAuth2AccessToken;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public String getClientId() {
        return clientId;
    }

    public OAuth2Authentication getAuthentication() {
        return authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
