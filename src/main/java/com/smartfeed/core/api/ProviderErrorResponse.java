package com.smartfeed.core.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Khamidullin Kamil
 * Date: 25.11.14
 * Time: 22:15
 */
@XmlRootElement
public class ProviderErrorResponse extends ErrorResponse {
    private String providerId;

    public ProviderErrorResponse(String providerId, ErrorResponse errorResponse) {
        super(errorResponse.getError(), errorResponse.getErrorDescription(), errorResponse.getApplicationMessage());
        this.providerId = providerId;
    }

    public ProviderErrorResponse(String providerId, String errorCode, String consumerMessage, String applicationMessage) {
        super(errorCode, consumerMessage, applicationMessage);
        this.providerId = providerId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
