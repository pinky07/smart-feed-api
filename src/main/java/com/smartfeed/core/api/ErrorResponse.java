package com.smartfeed.core.api;



import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 24/04/2013
 */
@XmlRootElement
public class ErrorResponse {
    private String error;
    private String errorDescription;
    private String applicationMessage;
    private List<ValidationError> validationErrors = new ArrayList<ValidationError>();

    public ErrorResponse() {}

    public ErrorResponse(String errorCode, String consumerMessage, String applicationMessage) {
        this.error = errorCode;
        this.errorDescription = consumerMessage;
        this.applicationMessage = applicationMessage;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getApplicationMessage() {
        return applicationMessage;
    }

    public void setApplicationMessage(String applicationMessage) {
        this.applicationMessage = applicationMessage;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }
}