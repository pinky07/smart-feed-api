package com.smartfeed.core.security;

import com.smartfeed.endpoints.user.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.UserIdSource;

/**
 * Author: Khamidullin Kamil
 * Date: 07.11.14
 * Time: 22:56
 */
public class AuthenticatedUserIdSource implements UserIdSource {

    public String getUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
        }

        if (authentication.getPrincipal() instanceof User) {
            return ((User) authentication.getPrincipal()).getId();
        }

        return authentication.getName();
    }

}