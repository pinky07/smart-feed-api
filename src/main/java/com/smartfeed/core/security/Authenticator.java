package com.smartfeed.core.security;

import com.smartfeed.endpoints.user.model.User;
import org.springframework.security.core.Authentication;

/**
 * Author: Khamidullin Kamil
 * Date: 02.12.14
 * Time: 19:40
 */
public interface Authenticator {
    Authentication authenticate(User user);
}
