package com.smartfeed.core.exception;

/**
 * Author: Khamidullin Kamil
 * Date: 15.12.14
 * Time: 14:29
 */
public class ProviderNotFoundException extends BaseWebApplicationException{
    public ProviderNotFoundException() {
        super(404, "EXCEPTION_PROVIDER_NOT_FOUND", "Provider not found", "No provider could be found for that Id");
    }
}
