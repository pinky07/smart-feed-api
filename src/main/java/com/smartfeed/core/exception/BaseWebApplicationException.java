package com.smartfeed.core.exception;

import com.smartfeed.core.api.ErrorResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @version 1.0
 * @author: Iain Porter
 * @since 24/04/2013
 */
public abstract class BaseWebApplicationException extends WebApplicationException {

    private final int status;
    private final String errorCode;
    private final String errorMessage;
    private final String developerMessage;

    public BaseWebApplicationException(int httpStatus, String errorMessage, String developerMessage) {
        this.status = httpStatus;
        this.errorCode = "Unknown";
        this.errorMessage = errorMessage;
        this.developerMessage = developerMessage;
    }

    public BaseWebApplicationException(int httpStatus, String errorCode, String errorMessage, String developerMessage) {
        this.status = httpStatus;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.developerMessage = developerMessage;
    }

    @Override
    public Response getResponse() {
        return Response.status(status).type(MediaType.APPLICATION_JSON_TYPE).entity(getErrorResponse()).build();
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }

    public ErrorResponse getErrorResponse() {
        ErrorResponse response = new ErrorResponse();
        response.setApplicationMessage(developerMessage);
        response.setErrorDescription(errorMessage);
        response.setError(errorCode);

        return response;
    }
}
