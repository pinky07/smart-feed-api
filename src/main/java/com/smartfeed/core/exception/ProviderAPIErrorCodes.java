package com.smartfeed.core.exception;

import org.springframework.social.*;

/**
 * Author: Khamidullin Kamil
 * Date: 25.11.14
 * Time: 23:32
 */
public class ProviderAPIErrorCodes {
    public static final String ERROR_PROVIDER_UNAUTHORIZED = "10000";
    public static final String ERROR_PROVIDER_OPERATION_NOT_PERMITTED = "10001";
    public static final String ERROR_PROVIDER_RESOURCE_NOT_FOUND = "10002";
    public static final String ERROR_PROVIDER_RATE_LIMIT_EXCEEDED = "10003";
    public static final String ERROR_PROVIDER_SERVER_EXCEPTION = "10004";
    public static final String ERROR_PROVIDER_UNCATEGORIZED = "10005";

    public static String errorCodeForException(ApiException exception) {
        if(exception instanceof NotAuthorizedException) {
            return ERROR_PROVIDER_UNAUTHORIZED;
        } else if(exception instanceof OperationNotPermittedException) {
            return ERROR_PROVIDER_OPERATION_NOT_PERMITTED;
        } else if(exception instanceof ResourceNotFoundException) {
            return ERROR_PROVIDER_RESOURCE_NOT_FOUND;
        } else if(exception instanceof RateLimitExceededException) {
            return ERROR_PROVIDER_RATE_LIMIT_EXCEEDED;
        } else if(exception instanceof ServerException) {
            return ERROR_PROVIDER_SERVER_EXCEPTION;
        } else {
            return ERROR_PROVIDER_UNCATEGORIZED;
        }
    }
}
