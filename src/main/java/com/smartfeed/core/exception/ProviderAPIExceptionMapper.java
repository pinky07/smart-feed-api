package com.smartfeed.core.exception;

import com.smartfeed.core.api.ProviderErrorResponse;
import org.springframework.social.ApiException;
import org.springframework.social.vkontakte.api.*;
import org.springframework.social.vkontakte.api.Error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Author: Khamidullin Kamil
 * Date: 25.11.14
 * Time: 23:04
 */
public class ProviderAPIExceptionMapper implements ExceptionMapper<ApiException> {
    @Override
    public Response toResponse(ApiException exception) {
        if(exception instanceof VKontakteErrorException) {
            return handleVKException((VKontakteErrorException)exception);
        }

        String providerId = exception.getProviderId();

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ProviderErrorResponse(providerId, ProviderAPIErrorCodes.errorCodeForException(exception),
                        providerId + " provider exception", exception.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    private Response handleVKException(VKontakteErrorException exception) {
        String providerId = exception.getProviderId();
        Error error = exception.getError();
        ProviderErrorResponse providerErrorResponse;
        if(error != null) {
            if(error.getCode().equals("5")) {
                providerErrorResponse = new ProviderErrorResponse(providerId, ProviderAPIErrorCodes.ERROR_PROVIDER_UNAUTHORIZED,
                        "You do not have the appropriate privileges to access " + providerId + " provider resource", error.getMessage());
            } else {
                providerErrorResponse = new ProviderErrorResponse(providerId, "500", "", error.getMessage());
            }

        } else {
            providerErrorResponse = new ProviderErrorResponse(providerId, "500", "", "Unknown " + providerId + " provider error");
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(providerErrorResponse)
                .type(MediaType.APPLICATION_JSON).
                        build();
    }
}
