package com.smartfeed.configuration;

import com.smartfeed.core.mail.MailSenderService;
import com.smartfeed.endpoints.user.dao.UserDao;
import com.smartfeed.endpoints.user.model.UserRepository;
import com.smartfeed.endpoints.user.model.UserRepositoryImpl;
import com.smartfeed.endpoints.user.model.VerificationTokenRepository;
import com.smartfeed.endpoints.user.resource.MeResource;
import com.smartfeed.endpoints.user.resource.PasswordResource;
import com.smartfeed.endpoints.user.resource.UserResource;
import com.smartfeed.endpoints.user.resource.VerificationResource;
import com.smartfeed.endpoints.user.service.UserService;
import com.smartfeed.endpoints.user.service.UserServiceImpl;
import com.smartfeed.endpoints.user.service.VerificationTokenService;
import com.smartfeed.endpoints.user.service.VerificationTokenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.validation.Validator;

@Configuration
public class UserConfiguration {
    
    @Autowired
    private UserDao userDao;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    private Validator validator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Resource(name="tokenServices")
    private DefaultTokenServices tokenServices;

    @Bean
    public VerificationTokenService verificationTokenService() {
        return new VerificationTokenServiceImpl(userDao, verificationTokenRepository, mailSenderService, validator, passwordEncoder);
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepositoryImpl(userDao);
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl(userRepository(), validator, passwordEncoder);
    } 
    
    @Bean
    public UserResource userResource() {
        return new UserResource(userService(), verificationTokenService(), tokenServices, passwordEncoder);
    }

    @Bean
    public PasswordResource passwordResource() {
        return new PasswordResource();
    }

    @Bean
    public VerificationResource verificationResource() {
        return new VerificationResource();
    }

    @Bean
    public MeResource meResource() {
        return new MeResource();
    }

}
