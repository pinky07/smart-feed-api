package com.smartfeed.configuration;

import com.smartfeed.core.exception.AuthExceptionTranslator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

/**
 * Author: Khamidullin Kamil
 * Date: 15.02.15
 * Time: 21:32
 */
@Component
@Scope("singleton")
@Lazy(false)
public class ExceptionTranslatorFix implements InitializingBean {
    @Autowired
    private TokenEndpoint tokenEndpoint;

    @Override
    public void afterPropertiesSet() throws Exception {
        tokenEndpoint.setProviderExceptionHandler(new AuthExceptionTranslator());
    }
}
