package com.smartfeed.configuration;

import com.smartfeed.endpoints.connection.*;
import com.smartfeed.core.security.AuthenticatedUserIdSource;
import com.smartfeed.core.security.Authenticator;
import com.smartfeed.core.security.AuthenticatorService;
import com.smartfeed.endpoints.connection.resource.ConnectionConnectController;
import com.smartfeed.endpoints.connection.resource.ConnectionSignInController;
import com.smartfeed.endpoints.feed.dao.FavoritesFeedDao;
import com.smartfeed.endpoints.feed.dao.HomeFeedDao;
import com.smartfeed.endpoints.feed.dao.UserFeedDao;
import com.smartfeed.endpoints.feed.model.FeedRepository;
import com.smartfeed.endpoints.feed.model.FeedRepositoryImpl;
import com.smartfeed.endpoints.signin.SimpleSignInAdapter;
import com.smartfeed.endpoints.user.dao.UserDao;
import com.smartfeed.social.APILocator;
import com.smartfeed.social.APILocatorImpl;
import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.facebook.FacebookConnector;
import com.smartfeed.social.twitter.TwitterConnector;
import com.smartfeed.social.vk.VkontakteConnector;
import net.exacode.spring.social.connect.SocialConnectionDao;
import net.exacode.spring.social.connect.mongo.MongoConnectionConverter;
import net.exacode.spring.social.connect.mongo.MongoConnectionDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.*;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.instagram.api.Instagram;
import org.springframework.social.instagram.connect.InstagramConnectionFactory;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.social.vkontakte.api.VK;
import org.springframework.social.vkontakte.connect.VKConnectionFactory;

import javax.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 02.11.14
 * Time: 22:22
 */
@EnableSocial
@Configuration
public class SocialConfiguration implements SocialConfigurer {

    // twitter
    @Value("${twitter.appKey}")
    private String twitterAppKey;
    @Value("${twitter.appSecret}")
    private String twitterAppSecret;

    // facebook
    @Value("${facebook.appKey}")
    private String facebookAppKey;
    @Value("${facebook.appSecret}")
    private String facebookAppSecret;

    // vk
    @Value("${vk.appKey}")
    private String vkAppKey;
    @Value("${vk.appSecret}")
    private String vkAppSecret;

    //instagram
    @Value("${instagram.appKey}")
    private String instagramAppKey;
    @Value("${instagram.appSecret}")
    private String instagramAppSecret;

    @Inject
    MongoTemplate mongoTemplate;

    @Inject
    ConnectionStatusDao connectionStatusDao;

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
        connectionFactoryConfigurer.addConnectionFactory(new TwitterConnectionFactory(twitterAppKey, twitterAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new FacebookConnectionFactory(facebookAppKey, facebookAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new VKConnectionFactory(vkAppKey, vkAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new InstagramConnectionFactory(instagramAppKey, instagramAppSecret));
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(5);
        pool.setMaxPoolSize(10);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }

    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticatedUserIdSource();
    }

    @Bean
    public SocialConnectionDao connectionService(MongoTemplate mongoOperations, ConnectionFactoryLocator connectionFactoryLocator) {
        return new MongoConnectionDao(mongoOperations, new MongoConnectionConverter(connectionFactoryLocator, Encryptors.noOpText()));
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return new UsersConnectionService(connectionService(mongoTemplate, connectionFactoryLocator), connectionStatusDao, connectionFactoryLocator);
    }

    @Bean
    public ConnectController connectController(ConnectionFactoryLocator connectionFactoryLocator, ConnectionRepository connectionRepository) {
        ConnectController connectController = new ConnectionConnectController(connectionFactoryLocator, connectionRepository);
        return connectController;
    }

    @Bean
    public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository usersConnectionRepository, UserDao userRepository, Authenticator authenticator) {
        ProviderSignInController signInController =
                new ConnectionSignInController(connectionFactoryLocator, usersConnectionRepository,
                        new SimpleSignInAdapter(new HttpSessionRequestCache(), userRepository, authenticator));
        return signInController;
    }

    @Bean
    public Authenticator authenticator() {
        return new AuthenticatorService();
    }

    @Bean
    @Scope(value="request", proxyMode= ScopedProxyMode.INTERFACES)
    public ConnectionStatusService connectionStatusService(UserIdSource userIdSource, ConnectionStatusDao connectionStatusDao) {
        return new ConnectionStatusServiceImpl(connectionStatusDao, userIdSource.getUserId());
    }

    @Bean(name = "home_feed_repository")
    @Scope(value="request", proxyMode= ScopedProxyMode.INTERFACES)
    public FeedRepository homeFeedRepository(UserIdSource userIdSource, ConnectionStatusService connectionStatusService, MongoOperations mongoOperations) {
        return new FeedRepositoryImpl(userIdSource.getUserId(), connectionStatusService.getEnabledProviders(), new HomeFeedDao(mongoOperations));
    }

    @Bean(name = "favorites_feed_repository")
    @Scope(value="request", proxyMode= ScopedProxyMode.INTERFACES)
    public FeedRepository favoritesFeedRepository(UserIdSource userIdSource, ConnectionStatusService connectionStatusService, MongoOperations mongoOperations) {
        return new FeedRepositoryImpl(userIdSource.getUserId(), connectionStatusService.getEnabledProviders(), new FavoritesFeedDao(mongoOperations));
    }

    @Bean(name = "user_feed_repository")
    @Scope(value="request", proxyMode= ScopedProxyMode.INTERFACES)
    public FeedRepository userFeedRepository(UserIdSource userIdSource, ConnectionStatusService connectionStatusService, MongoOperations mongoOperations) {
        return new FeedRepositoryImpl(userIdSource.getUserId(), connectionStatusService.getEnabledProviders(), new UserFeedDao(mongoOperations));
    }

    @Bean
    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
    public APILocator apiLocator(ConnectionRepository repository, ConnectionStatusService connectionStatusService) {
        APILocatorImpl apiLocator = new APILocatorImpl();

        // add facebook
        Connection<Facebook> facebookConnection = repository.findPrimaryConnection(Facebook.class);
        if(facebookConnection != null && connectionStatusService.isEnable(SocialNetwork.facebook.getProviderId())) {
            Facebook facebook = facebookConnection.getApi();
            apiLocator.registerAPI(new FacebookConnector(facebook));
        }

        // add twitter
        Connection<Twitter> twitterConnection = repository.findPrimaryConnection(Twitter.class);
        if(twitterConnection != null && connectionStatusService.isEnable(SocialNetwork.twitter.getProviderId())) {
            apiLocator.registerAPI(new TwitterConnector(twitterConnection.getApi()));
        }

        // add vk
        Connection<VK> vkConnection = repository.findPrimaryConnection(VK.class);
        if(vkConnection != null && connectionStatusService.isEnable(SocialNetwork.vkontakte.getProviderId())) {
            apiLocator.registerAPI(new VkontakteConnector(vkConnection.getApi()));
        }

//        Connection<Instagram> instagramConnection = repository.findPrimaryConnection(Instagram.class);
//
//
//
//        apiLocator.registerAPI(new SFVKontakteAPIAdapter(vk));
//        apiLocator.registerAPI(new SFTwitterAPIAdapter(twitter));

        return apiLocator;
    }

    @Bean
    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
    public Facebook facebook(ConnectionRepository repository) {
        Connection<Facebook> connection = repository.findPrimaryConnection(Facebook.class);
        return connection != null ? connection.getApi() : null;
    }

    @Bean
    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
    public Twitter twitter(ConnectionRepository repository) {
        Connection<Twitter> connection = repository.findPrimaryConnection(Twitter.class);
        return connection != null ? connection.getApi() : null;
    }

//    @Bean
//    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
//    public LinkedIn linkedin(ConnectionRepository repository) {
//        Connection<LinkedIn> connection = repository.findPrimaryConnection(LinkedIn.class);
//        return connection != null ? connection.getApi() : null;
//    }

    @Bean
    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
    public VK vkontakte(ConnectionRepository repository) {
        Connection<VK> connection = repository.findPrimaryConnection(VK.class);
        return connection != null ? connection.getApi() : null;
    }

    @Bean
    @Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
    public Instagram instagram(ConnectionRepository repository) {
        Connection<Instagram> connection = repository.findPrimaryConnection(Instagram.class);
        return connection != null ? connection.getApi() : null;
    }
}
