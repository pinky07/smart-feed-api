package com.smartfeed.social;

import com.smartfeed.social.core.feed.FeedOperations;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 19:04
 */
public interface APIConnector {
    FeedOperations feedOperations();

    String getProviderId();
}
