package com.smartfeed.social;

import com.smartfeed.social.APIConnector;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 18:48
 */
public interface APILocator {
    public APIConnector getAPIFor(String providerId);

    public List<APIConnector> getConnectedAPIs();
}
