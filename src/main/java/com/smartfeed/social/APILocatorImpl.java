package com.smartfeed.social;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 22:34
 */
public class APILocatorImpl implements APILocator {
    List<APIConnector> apis = new ArrayList<>();

    public void registerAPI(APIConnector api) {
        apis.add(api);
    }

    @Override
    public APIConnector getAPIFor(String providerId) {
        for(APIConnector api : apis) {
            if(api.getProviderId().equals(providerId)) {
                return api;
            }
        }

        return null;
    }

    @Override
    public List<APIConnector> getConnectedAPIs() {
        return apis;
    }
}
