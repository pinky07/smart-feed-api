package com.smartfeed.social.facebook.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Author: Khamidullin Kamil
 * Date: 25.11.14
 * Time: 22:44
 */
public class FacebookExceptionMapper implements ExceptionMapper {
    @Override
    public Response toResponse(Throwable exception) {
        return null;
    }
}
