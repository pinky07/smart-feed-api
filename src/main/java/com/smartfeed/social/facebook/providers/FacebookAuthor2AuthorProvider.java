package com.smartfeed.social.facebook.providers;

import com.smartfeed.social.core.feed.Author;
import org.springframework.social.facebook.api.Reference;

/**
 * Author: Khamidullin Kamil
 * Date: 22.11.14
 * Time: 21:37
 */
public class FacebookAuthor2AuthorProvider implements Author.AuthorDataProvider {
    private Reference reference;

    public FacebookAuthor2AuthorProvider(Reference reference) {
        this.reference = reference;
    }

    @Override
    public String getExternalId() {
        return reference.getId();
    }

    @Override
    public String getUsername() {
        return reference.getName();
    }

    @Override
    public String getFullName() {
        return reference.getName();
    }

    @Override
    public String getAvatarUrl() {
        return "https://graph.facebook.com/" + reference.getId() + "/picture?type=large";
    }
}
