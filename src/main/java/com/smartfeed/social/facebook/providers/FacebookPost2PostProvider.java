package com.smartfeed.social.facebook.providers;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.core.feed.Author;
import com.smartfeed.social.core.feed.Photo;
import org.springframework.social.facebook.api.Post;

import static com.smartfeed.social.core.feed.Post.PostProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 22:44
 */
public class FacebookPost2PostProvider implements PostProvider {
    private Post post;

    public FacebookPost2PostProvider(Post post) {
        this.post = post;
    }

    @Override
    public String getExternalId() {
        return post.getId();
    }

    @Override
    public String getText() {
        return post.getMessage();
    }

    @Override
    public Date getCreatedDate() {
        return post.getCreatedTime();
    }

    @Override
    public int getCommentsCount() {
        return post.getCommentCount();
    }

    @Override
    public int getFavesCount() {
        Integer count = post.getLikeCount();
        return (count == null) ? 0 : count;
    }

    @Override
    public int getRepostsCount() {
        return post.getSharesCount();
    }

    @Override
    public List<Photo> getPhotos() {
        List<Photo> photos = new ArrayList<>();
        if(post.getPicture() != null) {
            photos.add(new Photo(new Photo.PhotoDataProvider() {
                @Override
                public String getExternalId() {
                    return null;
                }

                @Override
                public String getPreviewUrl() {
                    return post.getPicture();
                }

                @Override
                public String getDescription() {
                    return null;
                }

                @Override
                public String getUrl() {
                    return post.getPicture();
                }
            }));
        }
        return photos;
    }

    @Override
    public boolean canFave() {
        return false;
    }

    @Override
    public boolean canComment() {
        return false;
    }

    @Override
    public boolean canRepost() {
        return false;
    }

    @Override
    public boolean isFaved() {
        return false;
    }

    @Override
    public Author getAuthor() {
        return new Author(new FacebookAuthor2AuthorProvider(post.getFrom()));
    }

    @Override
    public boolean isRepost() {
        return false;
    }

    @Override
    public com.smartfeed.social.core.feed.Post getRepostedPost() {
        return null;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.facebook.getProviderId();
    }
}
