package com.smartfeed.social.facebook;

import com.smartfeed.social.core.feed.AbstractFeedOperations;
import com.smartfeed.social.facebook.providers.FacebookPost2PostProvider;
import org.springframework.social.facebook.api.FeedOperations;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.Post;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 22:34
 */
public class FacebookFeedOperations extends AbstractFeedOperations<Post> {
    FeedOperations feedOperations;

    public FacebookFeedOperations(FeedOperations feedOperations) {
        this.feedOperations = feedOperations;
    }

    @Override
    protected com.smartfeed.social.core.feed.Post convert(Post post) {
        return new com.smartfeed.social.core.feed.Post(new FacebookPost2PostProvider(post));
    }

    @Override
    protected List<Post> loadHomeFeed(int size) {
        return feedOperations.getHomeFeed(new PagingParameters(size, 0, null, null));
    }

    @Override
    protected List<Post> loadHomeFeed(com.smartfeed.social.core.feed.Post after, int size) {
        PagingParameters pagingParameters = new PagingParameters(size, 0, null, ((after.getOriginalTimeCreated().getTime()/1000)-1));
        return feedOperations.getHomeFeed(pagingParameters);
    }

    @Override
    protected List<Post> loadUserFeed(int size) {
        return feedOperations.getFeed(new PagingParameters(size, 0, null, null));
    }

    @Override
    protected List<Post> loadUserFeed(com.smartfeed.social.core.feed.Post after, int size) {
        PagingParameters pagingParameters = new PagingParameters(size, 0, null, ((after.getOriginalTimeCreated().getTime()/1000)-1));
        return feedOperations.getFeed(pagingParameters);
    }
}
