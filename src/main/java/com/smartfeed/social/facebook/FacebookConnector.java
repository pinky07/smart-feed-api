package com.smartfeed.social.facebook;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.APIConnector;
import com.smartfeed.social.core.feed.FeedOperations;
import org.springframework.social.facebook.api.Facebook;

/**
 * Author: Khamidullin Kamil
 * Date: 09.11.14
 * Time: 21:43
 */
public class FacebookConnector implements APIConnector {
    private Facebook facebook;
    private FeedOperations feedOperations;

    public FacebookConnector(Facebook facebook) {
        this.facebook = facebook;
        init();
    }

    private void init() {
        this.feedOperations = new FacebookFeedOperations(facebook.feedOperations());
    }

    @Override
    public FeedOperations feedOperations() {
        return feedOperations;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.facebook.getProviderId();
    }
}
