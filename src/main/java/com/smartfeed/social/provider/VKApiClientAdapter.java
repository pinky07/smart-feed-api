package com.smartfeed.social.provider;

/**
 * Author: Khamidullin Kamil
 * Date: 11.10.14
 * Time: 19:46
 */
public class VKApiClientAdapter/** implements SocialNetworkProvider*/ {
//    protected Api vkClient;
//    protected static VKApiClientAdapter instance;
//
//    public VKApiClientAdapter(Account account) {
//        vkClient = new Api(account.getToken(), "3890987");
//        instance = this;
//    }
//
//    @Override
//    public SocialNetwork getProviderId() {
//        return SocialNetwork.vk;
//    }
//
//    @Override
//    public Feed<Post> getPhotoFeed() throws Exception {
//        Bundle params = new Bundle();
//        params.put("filter", "photo");
//        return getHomeFeed(params);
//    }
//
//    @Override
//    public Feed<Post> getPhotoFeed(Bundle params) throws Exception {
//        params.put("filter", "photo");
//        return getHomeFeed(params);
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed() throws Exception {
//        return getHomeFeed(new Bundle());
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed(Bundle params) throws Exception {
//        Feed<Post> news = new Feed<Post>();
//        int count  		= params.getInt("count", DEFAULT_POST_COUNT);
//        int offset 		= params.getInt("offset", 0);
//        String from     = params.getString("from", "");
//        String filter 	= params.getString("filter", "post,photo,note");
//
//        Newsfeed newsfeed = vkClient.getNews(0L, count, 0L, offset, from, "", filter, 5, "", "");
//
//        for(NewsItem item : newsfeed.items) {
//            news.addItem(new Post(new VKPostDataProvider(item, newsfeed)));
//        }
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext(news.size() == count && count > 0);
//        pagination.setParams("count", count);
//        pagination.setParams("offset", newsfeed.new_offset);
//        pagination.setParams("from", newsfeed.new_from);
//
//        news.setPagination(pagination);
//
//        return news;
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed(Post sincePost) throws Exception {
//        Feed<Post> news = new Feed<Post>();
//        int count  		= DEFAULT_POST_COUNT;
//        int offset 		= 0;
//        String from     = "";
//        String filter 	= "post,photo,note";
//
//        Newsfeed newsfeed = vkClient.getNews(0L, count, sincePost.getDate(), offset, from, "", filter, 5, "", "");
//
//        for(NewsItem item : newsfeed.items) {
//            news.addItem(new Post(new VKPostDataProvider(item, newsfeed)));
//        }
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext(news.size() == count && count > 0);
//        pagination.setParams("count", count);
//        pagination.setParams("offset", newsfeed.new_offset);
//        pagination.setParams("from", newsfeed.new_from);
//
//        news.setPagination(pagination);
//
//        return news;
//    }
//
//    @Override
//    public Feed<Post> getFavoritesFeed(Bundle params) throws Exception {
//        int count  = params.getInt("count", 40);
//        int offset = params.getInt("offset", -1);
//
//        WallFeed messages = vkClient.getFavePosts(count, offset);
//        Feed<Post> feed = createFeedByWallFeed(messages);
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext((count + offset) < messages.count);
//        pagination.setParams("count", count);
//        pagination.setParams("offset", offset + count);
//
//        feed.setPagination(pagination);
//
//        return feed;
//    }
//
//    public void sendPost(Bundle params) throws Exception{
//        String text = params.getString("text");
//        String photoUri = params.getString("photo_uri");
//
//        //vkClient.createWallPost(text, photoUri);
//    }
//
//    @Override
//    public void fave(Post post) throws Exception {
//        vkClient.addLike(Long.parseLong(post.getAuthor().getExternalId()), Long.parseLong(post.getExternalId()), "post", null, null, null);
//    }
//
//    @Override
//    public void unFave(Post post) throws Exception {
//        vkClient.deleteLike(Long.parseLong(post.getAuthor().getExternalId()), "post", Long.parseLong(post.getExternalId()), null, null);
//    }
//
//    @Override
//    public Feed<Post> getMyFeed(Bundle params) throws Exception {
//        int count  		= params.getInt("count", 40);
//        int offset 		= params.getInt("offset", -1);
//
//        WallFeed wallFeed 	= vkClient.getWallMessages(0L, count, offset, "");
//        Feed<Post> feed 			= createFeedByWallFeed(wallFeed);
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext((count + offset) < wallFeed.count);
//        pagination.setParams("count", count);
//        pagination.setParams("offset", offset + count);
//
//        feed.setPagination(pagination);
//
//        return feed;
//    }
//
//    private Feed<Post> createFeedByWallFeed(WallFeed wallFeed) {
//        Feed<Post> news = new Feed<Post>();
//        for (WallMessage wallMessage: wallFeed.items) {
//            news.addItem(new Post(new VKWallMessageDataProvider(wallMessage, wallFeed)));
//        }
//
//        return news;
//    }
//
//    private List<Author> getUsersByIds(Long...ids) throws Exception {
//        List<Author> users = new ArrayList<Author>();
//        List<com.perm.kate.api.User> profiles = vkClient.getProfiles(Arrays.asList(ids), null, null, "nom", null, null);
//        for(com.perm.kate.api.User profile : profiles) {
//            users.add(new Author(new VKUserDataProvider(profile)));
//        }
//
//        return users;
//    }
//
//    @Override
//    public Feed<Comment> getComments(Bundle params) throws Exception {
//        long ownerId = Long.parseLong(params.getString("owner_id"));
//        long postId  = Long.parseLong(params.getString("post_id"));
//        int offset   = params.getInt("offset", 0);
//        int count    = params.getInt("offset", 20);
//        String sort  = (params.containsKey("sort")) ? params.getString("sort") : "desc";
//
//        Feed<Comment> commentFeed = new Feed<Comment>();
//        CommentList commentList = vkClient.getWallComments(ownerId, postId, offset, count, sort.equals("desc"));
//        List<com.perm.kate.api.User> users = vkClient.getProfiles(Arrays.asList(commentList.getProfilesIds()), null, null, "nom", null, null);
//        List<Group> groups = vkClient.getGroups(Arrays.asList(commentList.getGroupsIds()), null, null);
//        for (com.perm.kate.api.Comment comment : commentList.comments){
//            commentFeed.addItem(new Comment(new VKCommentDataProvider(comment, users, groups)));
//        }
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext((count + offset) < commentList.count);
//        pagination.setParams("count", count);
//        pagination.setParams("sort", sort);
//        pagination.setParams("offset", offset + count);
//
//        commentFeed.setPagination(pagination);
//
//        return commentFeed;
//    }
//
//    @Override
//    public Comment addComment(Bundle params) throws Exception {
//        String text 	= params.getString("text");
//        long authorId 	= Long.parseLong(params.getString("author_id"));
//        long postId 	= Long.parseLong(params.getString("post_id"));
//
//        vkClient.createWallComment(authorId, postId, text, null, null, false, null, null);
//
//
//        return null;
//    }
//
//    public static class VKPhotoDataProvider implements Photo.PhotoDataProvider {
//        private com.perm.kate.api.Photo photo;
//
//        public VKPhotoDataProvider(com.perm.kate.api.Photo photo) {
//            this.photo = photo;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(photo.pid);
//        }
//
//        @Override
//        public String getPreviewUrl() {
//            return photo.src_small;
//        }
//
//        @Override
//        public String getUrl() {
//            return photo.src_big;
//        }
//    }
//
//    public static class VKUserDataProvider implements Author.AuthorDataProvider{
//        private com.perm.kate.api.User user;
//
//        public VKUserDataProvider(com.perm.kate.api.User user) {
//            this.user = user;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(user.uid);
//        }
//
//        @Override
//        public String getUsername() {
//            return user.nickname;
//        }
//
//        @Override
//        public String getFullName() {
//            return String.format("%s %s", user.first_name, user.last_name);
//        }
//
//        @Override
//        public String getAvatarUrl() {
//            return user.photo_medium_rec;
//        }
//    }
//
//    public static class VKGroupDataProvider implements Author.AuthorDataProvider {
//        private Group group;
//        public VKGroupDataProvider(Group group) {
//            this.group = group;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(group.gid*(-1));
//        }
//
//        @Override
//        public String getUsername() {
//            return group.name;
//        }
//
//        @Override
//        public String getFullName() {
//            return group.name;
//        }
//
//        @Override
//        public String getAvatarUrl() {
//            return group.photo_big;
//        }
//    }
//
//
//
//    public static class VKPostDataProvider implements Post.PostDataProvider {
//        private NewsItem newsItem;
//        private Newsfeed feed;
//        private Author author;
//
//        public VKPostDataProvider(NewsItem item, Newsfeed feed) {
//            this.newsItem = item;
//            this.feed = feed;
//        }
//
//        @Override
//        public String getSNCode() {
//            return "vk";
//        }
//
//        @Override
//        public String getExternalId() {
//            Author author = getAuthor();
//            String authorId = (author != null) ? author.getExternalId() : "null";
//
//            return authorId + "_" + Long.toString(newsItem.post_id);
//        }
//
//        @Override
//        public String getText() {
//            return newsItem.text;
//        }
//
//        @Override
//        public long getDate() {
//            return newsItem.date;
//        }
//
//        @Override
//        public int getCommentsCount() {
//            return newsItem.comment_count;
//        }
//
//        @Override
//        public int getFavesCount() {
//            return newsItem.like_count;
//        }
//
//        @Override
//        public int getRepostsCount() {
//            return newsItem.reposts_count;
//        }
//
//        @Override
//        public List<Photo> getPhotos() {
//            List<Photo> photos = new ArrayList<Photo>();
//
//            if(newsItem.type.equals("photo")) {
//                for(com.perm.kate.api.Photo photo : newsItem.photos) {
//                    photos.add(new Photo(new VKPhotoDataProvider(photo)));
//                }
//            } else {
//                for(Attachment attachment : newsItem.attachments) {
//                    if(attachment.photo != null) {
//                        photos.add(new Photo(new VKPhotoDataProvider(attachment.photo)));
//                    }
//                }
//            }
//
//            return photos;
//        }
//
//        @Override
//        public boolean canFave() {
//            return true;
//        }
//
//        @Override
//        public boolean canComment() {
//            return newsItem.comment_can_post;
//        }
//
//        @Override
//        public boolean canRepost() {
//            return true;
//        }
//
//        @Override
//        public boolean isFaved() {
//            return newsItem.user_like;
//        }
//
//        @Override
//        public Author getAuthor() {
//            if(author == null) {
//                author = getAuthorById(newsItem.source_id);
//            }
//
//            return author;
//        }
//
//        private Author getAuthorById(long sourceId) {
//            if(sourceId > 0) {
//                for (com.perm.kate.api.User user : feed.getProfiles()) {
//                    if(user.uid == sourceId) {
//                        return new Author(new VKUserDataProvider(user));
//                    }
//                }
//            } else {
//                for (Group group : feed.getGroups()) {
//                    if(group.gid == sourceId*(-1)) {
//                        return new Author(new VKGroupDataProvider(group));
//                    }
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        public boolean isRepost() {
//            return newsItem.copy_history != null;
//        }
//
//        @Override
//        public Post getRepostedPost() {
//            if(newsItem.copy_history != null) {
//                return new Post(new VKWallMessageDataProvider(newsItem.copy_history.get(0), feed));
//            }
//
//            return null;
//        }
//
//        @Override
//        public String getNextExternalId() {
//            int index = feed.items.indexOf(newsItem);
//            int nextIndex = index - 1;
//
//            if(nextIndex >= 0) {
//                NewsItem nextNewsItem = feed.items.get(nextIndex);
//
//                Author author = getAuthorById(nextNewsItem.source_id);
//                String authorId = (author != null) ? author.getExternalId() : "null";
//
//                return authorId + "_" + Long.toString(nextNewsItem.post_id);
//            } else {
//                return null;
//            }
//        }
//
//        @Override
//        public String getPrevExternalId() {
//            int index = feed.items.indexOf(newsItem);
//            int prevIndex = index + 1;
//            if(prevIndex < feed.items.size()) {
//                NewsItem prevNewsItem = feed.items.get(prevIndex);
//
//                Author author = getAuthorById(prevNewsItem.source_id);
//                String authorId = (author != null) ? author.getExternalId() : "null";
//
//                return authorId + "_" + Long.toString(prevNewsItem.post_id);
//            } else {
//                return null;
//            }
//        }
//    }
//
//    public static class VKWallMessageDataProvider implements Post.PostDataProvider {
//        private WallMessage wallMessage;
//        private VKFeed feed;
//
//        public VKWallMessageDataProvider(WallMessage wallMessage, VKFeed feed) {
//            this.wallMessage = wallMessage;
//            this.feed = feed;
//        }
//
//        @Override
//        public String getSNCode() {
//            return "vk";
//        }
//
//        @Override
//        public String getExternalId() {
//            if(wallMessage.id == 0) {
//                return String.format("%d%d", wallMessage.from_id, wallMessage.date);
//            } else {
//                return Long.toString(wallMessage.id);
//            }
//        }
//
//        @Override
//        public String getText() {
//            return wallMessage.text;
//        }
//
//        @Override
//        public long getDate() {
//            return wallMessage.date;
//        }
//
//        @Override
//        public int getCommentsCount() {
//            return (int)wallMessage.comment_count;
//        }
//
//        @Override
//        public int getFavesCount() {
//            return wallMessage.like_count;
//        }
//
//        @Override
//        public int getRepostsCount() {
//            return wallMessage.reposts_count;
//        }
//
//        @Override
//        public List<Photo> getPhotos() {
//            List<Photo> photos = new ArrayList<Photo>();
//            for (Attachment attachment: wallMessage.attachments) {
//                if(attachment.photo != null) {
//                    photos.add(new Photo(new VKPhotoDataProvider(attachment.photo)));
//                }
//            }
//
//            return photos;
//        }
//
//        @Override
//        public boolean canFave() {
//            return wallMessage.can_like;
//        }
//
//        @Override
//        public boolean canComment() {
//            return wallMessage.comment_can_post;
//        }
//
//        @Override
//        public boolean canRepost() {
//            return wallMessage.like_can_publish;
//        }
//
//        @Override
//        public boolean isFaved() {
//            return wallMessage.user_like;
//        }
//
//        @Override
//        public Author getAuthor() {
//            if(wallMessage.from_id > 0) {
//                for (com.perm.kate.api.User user : feed.getProfiles()) {
//                    if(user.uid == wallMessage.from_id) {
//                        return new Author(new VKUserDataProvider(user));
//                    }
//                }
//            } else {
//                for (Group group : feed.getGroups()) {
//                    if(group.gid == wallMessage.from_id*(-1)) {
//                        return new Author(new VKGroupDataProvider(group));
//                    }
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        public boolean isRepost() {
//            return false;  //todo распарсить репосты
//        }
//
//        @Override
//        public Post getRepostedPost() {
//            return null;  //todo распарсить репосты
//        }
//
//
//        @Override
//        public String getNextExternalId() {
//            return null;
//        }
//
//        @Override
//        public String getPrevExternalId() {
//            return null;
//        }
//    }
//
//    private class VKCommentDataProvider implements Comment.CommentDataProvider {
//        private com.perm.kate.api.Comment comment;
//        private List<com.perm.kate.api.User> users;
//        private List<Group> groups;
//
//        public VKCommentDataProvider(com.perm.kate.api.Comment comment, List<com.perm.kate.api.User> users, List<Group> groups) {
//            this.comment = comment;
//            this.users = users;
//            this.groups = groups;
//        }
//
//        @Override
//        public Author getAuthor() {
//            if(comment.from_id > 0) {
//                for(com.perm.kate.api.User user : users) {
//                    if (user.uid == comment.from_id) {
//                        return new Author(new VKUserDataProvider(user));
//                    }
//                }
//            } else {
//                for(Group group : groups) {
//                    if (group.gid == comment.from_id*-1) {
//                        return new Author(new VKGroupDataProvider(group));
//                    }
//                }
//            }
//
//
//            return null;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(comment.cid);
//        }
//
//        @Override
//        public String getText() {
//            return comment.message;
//        }
//
//        @Override
//        public long getDate() {
//            return comment.date;
//        }
//
//        @Override
//        public String getSNCode() {
//            return "vk";
//        }
//    }
}
