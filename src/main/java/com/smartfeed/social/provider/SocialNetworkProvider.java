package com.smartfeed.social.provider;

import com.smartfeed.social.SocialNetwork;

/**
 * Author: Khamidullin Kamil
 * Date: 11.10.14
 * Time: 19:27
 */
public interface SocialNetworkProvider {
    public static int DEFAULT_POST_COUNT = 40;

    public SocialNetwork getSocialNetwork();

//    public Feed<Post> getHomeFeed() throws Exception;
//
//    public Feed<Post> getPhotoFeed() throws Exception;
//
//    public Feed<Post> getPhotoFeed(Bundle params) throws Exception;
//
//    public Feed<Post> getHomeFeed(Bundle params) throws Exception;
//
//    public Feed<Post> getHomeFeed(Post sincePost) throws Exception;
//
//    public Feed<Post> getFavoritesFeed(Bundle params) throws Exception;
//
//    public void fave(Post post) throws Exception;
//
//    public void unFave(Post post) throws Exception;
//
//    public void sendPost(Bundle params) throws Exception;
//
//    public Feed<Post> getMyFeed(Bundle params) throws Exception;
//
//    public Feed<Comment> getComments(Bundle params) throws Exception;
//
//    public Comment addComment(Bundle params) throws Exception;
}
