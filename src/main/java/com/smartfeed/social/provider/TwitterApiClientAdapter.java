package com.smartfeed.social.provider;

/**
 * Author: Khamidullin Kamil
 * Date: 15.10.14
 * Time: 0:22
 */
public class TwitterApiClientAdapter /**implements SocialNetworkProvider**/ {
    private static final String CONSUMER_KEY = "MPf9Oe5B71KxV5KzaNDacg";
    private static final String CONSUMER_SECRET = "OHt4FwED5WGD1FqijG3iiSdcnFvuKQIXezd0ZxJYRpg";
    private enum TimeLine {
        HOME, FAVORITE, USER
    }

//    Twitter twitterClient;
//
//    public TwitterApiClientAdapter(Account account) {
//        twitterClient = new TwitterFactory().getInstance();
//        twitterClient.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
//        twitterClient.setOAuthAccessToken(new AccessToken(account.getToken(), account.getSecret()));
//    }
//
//    @Override
//    public SocialNetwork getProviderId() {
//        return SocialNetwork.twitter;
//    }
//
//    @Override
//    public Feed<Post> getPhotoFeed() throws TwitterException {
//        return null;
//    }
//
//    @Override
//    public Feed<Post> getPhotoFeed(Bundle params) throws TwitterException {
//        return null;
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed() throws TwitterException {
//        return getHomeFeed(new Bundle());
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed(Post sincePost) throws Exception {
//        Bundle bundle = new Bundle();
//        bundle.put("max_id", sincePost.getExternalId());
//
//        return getHomeFeed(bundle);
//    }
//
//    @Override
//    public Feed<Post> getHomeFeed(Bundle params) throws TwitterException {
//        return getTimeLine(params, TimeLine.HOME);
//    }
//
//    @Override
//    public Feed<Post> getFavoritesFeed(Bundle params) throws Exception {
//        return getTimeLine(params, TimeLine.FAVORITE);
//    }
//
//    @Override
//    public void sendPost(Bundle params) throws Exception {
//        String text = params.getString("text");
//        String photoUri = params.getString("photo_uri");
//
//        StatusUpdate status = new StatusUpdate(text);
//        if(photoUri != null && !photoUri.isEmpty()) {
//            status.setMedia(new File(photoUri));
//        }
//
//        twitterClient.updateStatus(status);
//    }
//
//    @Override
//    public void fave(Post post) throws Exception {
//        twitterClient.createFavorite(Long.parseLong(post.getExternalId()));
//    }
//
//    @Override
//    public void unFave(Post post) throws Exception {
//        twitterClient.destroyFavorite(Long.parseLong(post.getExternalId()));
//    }
//
//    @Override
//    public Feed<Post> getMyFeed(Bundle params) throws Exception {
//        return getTimeLine(params, TimeLine.USER);
//    }
//
//    private Feed<Post> getTimeLine(Bundle params, TimeLine timeLine) throws TwitterException{
//        int count = params.getInt("count", DEFAULT_POST_COUNT);
//        String maxId = params.getString("max_id", "");
//
//        Paging paging = new Paging();
//
//        paging.setCount(count);
//
//        if(!maxId.isEmpty()) {
//            paging.setMaxId(Long.parseLong(maxId));
//        }
//
//        ResponseList<Status> statusList;
//        switch (timeLine) {
//            case USER:
//                statusList = twitterClient.getUserTimeline(paging);
//                break;
//            case FAVORITE:
//                statusList = twitterClient.getFavorites(paging);
//                break;
//            default:
//                statusList = twitterClient.getHomeTimeline(paging);
//        }
//
//        Feed<Post> feed = new Feed<Post>();
//        for (Status status: statusList) {
//            feed.addItem(new Post(new TwitterStatusDataProvider(status, statusList)));
//        }
//
//        Pagination pagination = new Pagination();
//        if(!feed.isEmpty()) {
//            pagination.setParams("max_id", ((statusList.get(statusList.size() - 1).getId()) - 1));
//            pagination.setHasNext(true);
//        } else {
//            pagination.setHasNext(false);
//        }
//
//        pagination.setParams("count", count);
//        feed.setPagination(pagination);
//
//        return feed;
//    }
//
//    @Override
//    public Feed<Comment> getComments(Bundle params) throws Exception {
//        long postId  = Long.parseLong(params.getString("post_id"));
//        String username  = params.getString("username");
//
//        Feed<Comment> feed = new Feed<Comment>();
//
//        Query query = new Query();
//        query.sinceId(postId).query("@"+username).count(100);
//
//        QueryResult result = twitterClient.search(query);
//        List<Status> tweets = result.getTweets();
//        for(Status status :tweets) {
//            if(status.getInReplyToStatusId() == postId) {
//                feed.addItem(new Comment(new TwitterCommentDataProvider(status)));
//            }
//        }
//
//        if(params.containsKey("sort") && params.getString("sort").equals("desc")) {
//            feed.sort();
//        }
//
//        Pagination pagination = new Pagination();
//        pagination.setHasNext(false);
//        feed.setPagination(pagination);
//
//        return feed;
//    }
//
//    @Override
//    public Comment addComment(Bundle params) throws Exception {
//        String text = params.getString("text");
//        String username = params.getString("username");
//        long postId = Long.parseLong(params.getString("post_id"));
//
//        StatusUpdate update = new StatusUpdate(String.format("@%s %s", username, text));
//        update.setInReplyToStatusId(postId);
//
//        Status status = twitterClient.updateStatus(update);
//
//        return new Comment(new TwitterCommentDataProvider(status));
//    }
//
//    private class TwitterPhotoDataProvider implements Photo.PhotoDataProvider {
//        private MediaEntity mediaEntity;
//
//        public TwitterPhotoDataProvider(MediaEntity mediaEntity) {
//            this.mediaEntity = mediaEntity;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(mediaEntity.getId());
//        }
//
//        @Override
//        public String getPreviewUrl() {
//            return mediaEntity.getMediaURL();
//        }
//
//        @Override
//        public String getUrl() {
//            return mediaEntity.getMediaURL();
//        }
//    }
//
//    private class TwitterUserDataProvider implements Author.AuthorDataProvider {
//        private twitter4j.User user;
//
//        public TwitterUserDataProvider(twitter4j.User user) {
//            this.user = user;
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(user.getId());
//        }
//
//        @Override
//        public String getUsername() {
//            return user.getScreenName();
//        }
//
//        @Override
//        public String getFullName() {
//            return user.getName();
//        }
//
//        @Override
//        public String getAvatarUrl() {
//            return user.getProfileImageURL();
//        }
//    }
//
//    private class TwitterStatusDataProvider implements Post.PostDataProvider {
//        private Status status;
//        private List<Status> statusList;
//
//        public TwitterStatusDataProvider(Status status, List<Status> statusList) {
//            this.status = status;
//            this.statusList = statusList;
//        }
//
//        @Override
//        public String getSNCode() {
//            return "twitter";
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(status.getId());
//        }
//
//        @Override
//        public String getText() {
//            return status.getText();
//        }
//
//        @Override
//        public long getDate() {
//            return status.getCreatedAt().getTime()/1000;
//        }
//
//        @Override
//        public int getCommentsCount() {
//            return -1;
//        }
//
//        @Override
//        public int getFavesCount() {
//            return -1;
//        }
//
//        @Override
//        public int getRepostsCount() {
//            return (int)status.getRetweetCount();
//        }
//
//        @Override
//        public List<Photo> getPhotos() {
//            List<Photo> photos = new ArrayList<Photo>();
//
//            for(MediaEntity mediaEntity : status.getMediaEntities()) {
//                if(mediaEntity.getType().equals("photo")) {
//                    photos.add(new Photo(new TwitterPhotoDataProvider(mediaEntity)));
//                }
//            }
//
//            return photos;
//        }
//
//        @Override
//        public boolean canFave() {
//            return true;
//        }
//
//        @Override
//        public boolean canComment() {
//            return true;
//        }
//
//        @Override
//        public boolean canRepost() {
//            return true;
//        }
//
//        @Override
//        public boolean isFaved() {
//            return status.isFavorited();
//        }
//
//        @Override
//        public Author getAuthor() {
//            return new Author(new TwitterUserDataProvider(status.getUser()));
//        }
//
//        @Override
//        public boolean isRepost() {
//            return status.isRetweet();
//        }
//
//        @Override
//        public Post getRepostedPost() {
//            if(status.getRetweetedStatus() != null) {
//                return new Post(new TwitterStatusDataProvider(status.getRetweetedStatus(), new ArrayList<Status>()));
//            } else {
//                return null;
//            }
//        }
//
//        @Override
//        public String getNextExternalId() {
//            if(statusList != null) {
//                int index = statusList.indexOf(status);
//                int nextIndex = index - 1;
//
//                if(nextIndex >= 0) {
//                    return Long.toString(statusList.get(nextIndex).getId());
//                } else {
//                    return null;
//                }
//            } else {
//                return null;
//            }
//        }
//
//        @Override
//        public String getPrevExternalId() {
//            if(statusList != null) {
//                int index = statusList.indexOf(status);
//                int prevIndex = index + 1;
//                if(prevIndex < statusList.size()) {
//                    return Long.toString(statusList.get(prevIndex).getId());
//                } else {
//                    return null;
//                }
//            } else {
//                return null;
//            }
//        }
//    }
//
//    private class TwitterCommentDataProvider implements Comment.CommentDataProvider {
//        private Status status;
//
//        public TwitterCommentDataProvider(Status status) {
//            this.status = status;
//        }
//
//        @Override
//        public String getSNCode() {
//            return "twitter";
//        }
//
//        @Override
//        public Author getAuthor() {
//            return new Author(new TwitterUserDataProvider(status.getUser()));
//        }
//
//        @Override
//        public String getExternalId() {
//            return Long.toString(status.getId());
//        }
//
//        @Override
//        public String getText() {
//            return status.getText();
//        }
//
//        @Override
//        public long getDate() {
//            return status.getCreatedAt().getTime()/1000;
//        }
//    }
}
