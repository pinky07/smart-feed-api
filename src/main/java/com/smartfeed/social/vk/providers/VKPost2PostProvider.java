package com.smartfeed.social.vk.providers;
import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.core.feed.Author;
import com.smartfeed.social.core.feed.Photo;
import org.springframework.social.vkontakte.api.Group;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.UserProfile;
import org.springframework.social.vkontakte.api.attachment.Attachment;
import org.springframework.social.vkontakte.api.attachment.PhotoAttachment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.smartfeed.social.core.feed.Post.PostProvider;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 21:24
 */
public class VKPost2PostProvider implements PostProvider {
    private Post post;

    public VKPost2PostProvider(Post post) {
        this.post = post;
    }

    @Override
    public String getExternalId() {
        return post.getId();
    }

    @Override
    public String getText() {
        return post.getText();
    }

    @Override
    public Date getCreatedDate() {
        return post.getDate();
    }

    @Override
    public int getCommentsCount() {
        Post.Comments comments = post.getComments();
        if(comments != null) {
            return comments.getCount();
        }

        return 0;
    }

    @Override
    public int getFavesCount() {
        Post.Likes likes = post.getLikes();
        if(likes != null) {
            return likes.getCount();
        }

        return 0;
    }

    @Override
    public int getRepostsCount() {
        Post.Reposts reposts = post.getReposts();
        if(reposts != null) {
            return reposts.getCount();
        }

        return 0;
    }

    @Override
    public List<Photo> getPhotos() {
        List<Photo> photos = new ArrayList<>();
        List<? extends Attachment> attachments = post.getAttachments();
        if(attachments != null) {
            for(Attachment attachment : post.getAttachments()) {
                if(attachment instanceof PhotoAttachment) {
                    photos.add(new Photo(new VKPhoto2PhotoProvider(((PhotoAttachment) attachment).getPhoto())));
                }
            }
        }

        return photos;
    }

    @Override
    public boolean canFave() {
        Post.Likes likes = post.getLikes();
        if(likes != null) {
            return likes.isCanLike();
        }

        return false;
    }

    @Override
    public boolean canComment() {
        Post.Comments comments = post.getComments();
        if(comments != null) {
            return comments.isCanPost();
        }

        return false;
    }

    @Override
    public boolean canRepost() {
        return true;
    }

    @Override
    public boolean isFaved() {
        Post.Likes likes = post.getLikes();
        if(likes != null) {
            return likes.isUserLikes();
        }

        return false;
    }

    @Override
    public Author getAuthor() {
        Object author = post.getAuthor();

        if(author != null) {
            if(author instanceof UserProfile) {
                return new Author(new VKUserProfile2AuthorProvider((UserProfile)author));
            } else if (author instanceof Group) {
                return new Author(new VKGroup2AuthorProvider((Group)author));
            }
        }

        return null;
    }

    @Override
    public boolean isRepost() {
        return false;
    }

    @Override
    public com.smartfeed.social.core.feed.Post getRepostedPost() {
        return null;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.vkontakte.getProviderId();
    }
}
