package com.smartfeed.social.vk.providers;

import com.smartfeed.social.core.feed.Author;
import org.springframework.social.vkontakte.api.Group;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 21:15
 */
public class VKGroup2AuthorProvider implements Author.AuthorDataProvider {
    private Group group;

    public VKGroup2AuthorProvider(Group group) {
        this.group = group;
    }

    @Override
    public String getExternalId() {
        return group.getId();
    }

    @Override
    public String getUsername() {
        return group.getScreenName();
    }

    @Override
    public String getFullName() {
        return group.getName();
    }

    @Override
    public String getAvatarUrl() {
        return group.getSrcBig();
    }
}
