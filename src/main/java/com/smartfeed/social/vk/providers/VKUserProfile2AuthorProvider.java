package com.smartfeed.social.vk.providers;

import com.smartfeed.social.core.feed.Author;
import org.springframework.social.vkontakte.api.UserProfile;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 21:18
 */
public class VKUserProfile2AuthorProvider implements Author.AuthorDataProvider {
    private UserProfile userProfile;

    public VKUserProfile2AuthorProvider(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public String getExternalId() {
        return userProfile.getId();
    }

    @Override
    public String getUsername() {
        return userProfile.getScreenName();
    }

    @Override
    public String getFullName() {
        return userProfile.getFirstName() + " " + userProfile.getLastName();
    }

    @Override
    public String getAvatarUrl() {
        return userProfile.getPhoto();
    }
}
