package com.smartfeed.social.vk.providers;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.core.feed.Author;
import com.smartfeed.social.core.feed.Photo;
import org.springframework.social.vkontakte.api.Group;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.UserProfile;
import org.springframework.social.vkontakte.api.attachment.Attachment;
import org.springframework.social.vkontakte.api.attachment.PhotoAttachment;
import org.springframework.social.vkontakte.api.news.NewsPost;
import static com.smartfeed.social.core.feed.Post.PostProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 20:00
 */
public class VKNewsPost2PostProvider implements PostProvider {
    private NewsPost newsPost;

    public VKNewsPost2PostProvider(NewsPost post) {
        this.newsPost = post;
    }

    @Override
    public String getExternalId() {
        return newsPost.getAuthorId() + "_" + newsPost.getId();
    }

    @Override
    public String getText() {
        return newsPost.getText();
    }

    @Override
    public Date getCreatedDate() {
        return newsPost.getCreatedDate();
    }

    @Override
    public int getCommentsCount() {
        Post.Comments comments = newsPost.getComments();
        if(comments != null) {
            return comments.getCount();
        }

        return 0;
    }

    @Override
    public int getFavesCount() {
        Post.Likes lakes = newsPost.getLikes();
        if(lakes != null) {
            return lakes.getCount();
        }

        return 0;
    }

    @Override
    public int getRepostsCount() {
        Post.Reposts reposts = newsPost.getReposts();
        if(reposts != null) {
            return reposts.getCount();
        }

        return 0;
    }

    @Override
    public List<Photo> getPhotos() {
        List<Photo> photos = new ArrayList<>();
        List<? extends Attachment> attachments = newsPost.getAttachments();
        if(attachments != null) {
            for(Attachment attachment : attachments) {
                if(attachment instanceof PhotoAttachment) {
                    photos.add(new Photo(new VKPhoto2PhotoProvider(((PhotoAttachment) attachment).getPhoto())));
                }
            }
        }

        return photos;
    }

    @Override
    public boolean canFave() {
        Post.Likes lakes = newsPost.getLikes();
        if(lakes != null) {
            return lakes.isCanLike();
        }

        return false;
    }

    @Override
    public boolean canComment() {
        Post.Comments comments = newsPost.getComments();
        if(comments != null) {
            return comments.isCanPost();
        }

        return false;
    }

    @Override
    public boolean canRepost() {
        return true;
    }

    @Override
    public boolean isFaved() {
        Post.Likes lakes = newsPost.getLikes();
        if(lakes != null) {
            return lakes.isUserLikes();
        }

        return false;
    }

    @Override
    public Author getAuthor() {
        Object author = newsPost.getAuthor();

        if(author != null) {
            if(author instanceof UserProfile) {
                return new Author(new VKUserProfile2AuthorProvider((UserProfile)author));
            } else if (author instanceof Group) {
                return new Author(new VKGroup2AuthorProvider((Group)author));
            }
        }

        return null;
    }

    @Override
    public boolean isRepost() {
        List<Post> copyHistory = newsPost.getCopyHistory();

        return copyHistory != null && copyHistory.size() > 0;
    }

    @Override
    public com.smartfeed.social.core.feed.Post getRepostedPost() {
        if(isRepost()) {
            List<Post> copyHistory = newsPost.getCopyHistory();
            Post post = copyHistory.get(0);

            return new com.smartfeed.social.core.feed.Post(new VKPost2PostProvider(post));
        }

        return null;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.vkontakte.getProviderId();
    }
}
