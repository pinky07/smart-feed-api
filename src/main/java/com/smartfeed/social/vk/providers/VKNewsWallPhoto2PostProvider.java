package com.smartfeed.social.vk.providers;
import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.core.feed.Author;
import com.smartfeed.social.core.feed.Photo;
import com.smartfeed.social.core.feed.Post;
import org.springframework.social.vkontakte.api.Group;
import org.springframework.social.vkontakte.api.UserProfile;
import org.springframework.social.vkontakte.api.news.NewsWallPhoto;
import org.springframework.social.vkontakte.api.news.Photos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.smartfeed.social.core.feed.Post.PostProvider;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 21:44
 */
public class VKNewsWallPhoto2PostProvider implements PostProvider {
    NewsWallPhoto wallPhoto;

    public VKNewsWallPhoto2PostProvider(NewsWallPhoto wallPhoto) {
        this.wallPhoto = wallPhoto;
    }

    @Override
    public String getExternalId() {
        return wallPhoto.getAuthorId() + "_" + wallPhoto.getId();
    }

    @Override
    public String getText() {
        return "";
    }

    @Override
    public Date getCreatedDate() {
        return wallPhoto.getCreatedDate();
    }

    @Override
    public int getCommentsCount() {
        return -1;
    }

    @Override
    public int getFavesCount() {
        return -1;
    }

    @Override
    public int getRepostsCount() {
        return -1;
    }

    @Override
    public List<Photo> getPhotos() {
        List<Photo> photoList = new ArrayList<>();
        Photos photos =  wallPhoto.getPhotos();
        if(photos != null && photos.getAttachments() != null) {
            for(org.springframework.social.vkontakte.api.attachment.Photo photo : photos.getAttachments()) {
                photoList.add(new Photo(new VKPhoto2PhotoProvider(photo)));
            }
        }

        return photoList;
    }

    @Override
    public boolean canFave() {
        return false;
    }

    @Override
    public boolean canComment() {
        return false;
    }

    @Override
    public boolean canRepost() {
        return false;
    }

    @Override
    public boolean isFaved() {
        return false;
    }

    @Override
    public Author getAuthor() {
        Object author = wallPhoto.getAuthor();

        if(author != null) {
            if(author instanceof UserProfile) {
                return new Author(new VKUserProfile2AuthorProvider((UserProfile)author));
            } else if (author instanceof Group) {
                return new Author(new VKGroup2AuthorProvider((Group)author));
            }
        }

        return null;
    }

    @Override
    public boolean isRepost() {
        return false;
    }

    @Override
    public Post getRepostedPost() {
        return null;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.vkontakte.getProviderId();
    }
}
