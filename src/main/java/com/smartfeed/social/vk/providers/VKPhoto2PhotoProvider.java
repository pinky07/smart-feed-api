package com.smartfeed.social.vk.providers;

import com.smartfeed.social.core.feed.Photo.PhotoDataProvider;
import org.springframework.social.vkontakte.api.attachment.Photo;


/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 20:56
 */
public class VKPhoto2PhotoProvider implements PhotoDataProvider {
    private Photo photo;

    public VKPhoto2PhotoProvider(Photo photo) {
        this.photo = photo;
    }

    @Override
    public String getExternalId() {
        return Long.toString(photo.getPhotoId());
    }

    @Override
    public String getPreviewUrl() {
        return photo.getPhoto130();
    }

    @Override
    public String getUrl() {
        return photo.getPhoto807();
    }

    @Override
    public String getDescription() {
        return null;
    }
}
