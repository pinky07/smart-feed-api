package com.smartfeed.social.vk;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.APIConnector;
import com.smartfeed.social.core.feed.FeedOperations;
import org.springframework.social.vkontakte.api.VK;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 19:29
 */
public class VkontakteConnector implements APIConnector {
    VK vk;
    FeedOperations feedOperations;

    public VkontakteConnector(VK vk) {
        this.vk = vk;
        init();
    }

    private void init() {
        feedOperations = new VKFeedOperations(vk.getFeedOperations(), vk.wallOperations());
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.vkontakte.getProviderId();
    }

    @Override
    public FeedOperations feedOperations() {
        return feedOperations;
    }
}
