package com.smartfeed.social.vk;

import com.smartfeed.social.core.feed.AbstractFeedOperations;
import com.smartfeed.social.core.feed.Meta;
import com.smartfeed.social.core.feed.Post;
import com.smartfeed.social.vk.providers.VKNewsWallPhoto2PostProvider;
import com.smartfeed.social.vk.providers.VKNewsPost2PostProvider;
import org.springframework.social.vkontakte.api.IFeedOperations;
import org.springframework.social.vkontakte.api.IWallOperations;
import org.springframework.social.vkontakte.api.news.NewsItem;
import org.springframework.social.vkontakte.api.news.NewsPost;
import org.springframework.social.vkontakte.api.news.NewsWallPhoto;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 19:52
 */
public class VKFeedOperations extends AbstractFeedOperations<NewsItem> {
    private IFeedOperations feedTemplate;
    private IWallOperations wallOperations;

    public VKFeedOperations(IFeedOperations feedTemplate, IWallOperations wallOperations) {
        this.feedTemplate = feedTemplate;
        this.wallOperations = wallOperations;
    }

    @Override
    protected Post convert(NewsItem post) {
        Post p;

        if(post instanceof NewsPost) {
            p = new Post(new VKNewsPost2PostProvider((NewsPost)post));
        } else {
            p = new Post(new VKNewsWallPhoto2PostProvider((NewsWallPhoto)post));
        }

        if(post.getFrom() != null) {
            p.getMeta()
             .addParam("offset", post.getFrom());
        }

        return p;
    }

    @Override
    protected List<NewsItem> loadHomeFeed(int size) {
        return feedTemplate.getFeed(size);
    }

    @Override
    protected List<NewsItem> loadHomeFeed(Post after, int size) {
        Meta meta = after.getMeta();
        if(meta.hasPagingParam("offset")) {
            return feedTemplate.getFeed(meta.getParam("offset"), size);
        } else {
            long startTime = after.getOriginalTimeCreated().getTime();
            return feedTemplate.getFeed(0, (startTime/1000)-1, size);
        }
    }

    private List<NewsItem> wrapWallPosts(List<org.springframework.social.vkontakte.api.Post> wallPosts) {
        List<NewsItem> newsItems = new ArrayList<>();
        for(org.springframework.social.vkontakte.api.Post wallPost : wallPosts) {
            newsItems.add(new VKNewsPostWrapper(wallPost));
        }
        return newsItems;
    }

    @Override
    protected List<NewsItem> loadUserFeed(int size) {
        return wrapWallPosts(wallOperations.getPosts(0, size));
    }

    @Override
    protected List<NewsItem> loadUserFeed(Post after, int size) {
        Meta meta = after.getMeta();
        if(meta.hasPagingParam("offset")) {
            int offset = Integer.parseInt(meta.getParam("offset"));
            List<org.springframework.social.vkontakte.api.Post> wallPosts = wallOperations.getPosts(offset, size);
            if(wallPosts.isEmpty()) {
                return new ArrayList<>();
            }

            org.springframework.social.vkontakte.api.Post lastPost = wallPosts.get(wallPosts.size()-1);

            if(!after.getOriginalTimeCreated().after(lastPost.getDate())) {
                int newOffset = offset + wallPosts.size();
                after.getMeta().addParam("offset", String.valueOf(newOffset));

                return loadUserFeed(after, size);
            }

            return wrapWallPosts(wallPosts);
        }

        return new ArrayList<>();
    }
}
