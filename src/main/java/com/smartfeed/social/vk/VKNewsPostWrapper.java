package com.smartfeed.social.vk;

import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.attachment.Attachment;
import org.springframework.social.vkontakte.api.news.NewsItemType;
import org.springframework.social.vkontakte.api.news.NewsPost;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 15.12.14
 * Time: 18:10
 */
public class VKNewsPostWrapper extends NewsPost {
    private Post wallPost;

    public VKNewsPostWrapper(Post wallPost) {
        this.wallPost = wallPost;
    }

    @Override
    public String getText() {
        return wallPost.getText();
    }

    @Override
    public List<? extends Attachment> getAttachments() {
        return wallPost.getAttachments();
    }

    @Override
    public List<Post> getCopyHistory() {
        return wallPost.getCopyHistory();
    }

    @Override
    public Post.Likes getLikes() {
        return super.getLikes();
    }

    @Override
    public Post.Reposts getReposts() {
        return wallPost.getReposts();
    }

    @Override
    public Post.Comments getComments() {
        return wallPost.getComments();
    }

    @Override
    public String getId() {
        return wallPost.getId();
    }

    @Override
    public NewsItemType getType() {
        return super.getType();
    }

    @Override
    public Object getAuthor() {
        return wallPost.getAuthor();
    }

    @Override
    public String getAuthorId() {
        return wallPost.getAuthorId();
    }

    @Override
    public Date getCreatedDate() {
        return wallPost.getDate();
    }

    @Override
    public String getFrom() {
        if(wallPost.getPadding() > 0) {
            return String.valueOf(wallPost.getPadding());
        }

        return null;
    }
}
