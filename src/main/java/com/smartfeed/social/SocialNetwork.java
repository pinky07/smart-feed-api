package com.smartfeed.social;

/**
 * Author: Khamidullin Kamil
 * Date: 26.10.14
 * Time: 18:52
 */
public enum SocialNetwork {
    vkontakte("vkontakte"),
    twitter("twitter"),
    facebook("facebook");

    String providerId;

    SocialNetwork(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderId() {
        return this.providerId;
    }
}
