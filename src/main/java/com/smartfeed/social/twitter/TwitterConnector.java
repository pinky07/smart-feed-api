package com.smartfeed.social.twitter;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.APIConnector;
import com.smartfeed.social.core.feed.FeedOperations;
import org.springframework.social.twitter.api.Twitter;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 19:15
 */
public class TwitterConnector implements APIConnector {
    private Twitter twitter;
    private FeedOperations feedOperations;

    public TwitterConnector(Twitter twitter) {
        this.twitter = twitter;
        init();
    }

    private void init() {
        feedOperations = new TwitterFeedOperations(twitter.timelineOperations());
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.twitter.getProviderId();
    }

    @Override
    public FeedOperations feedOperations() {
        return feedOperations;
    }
}
