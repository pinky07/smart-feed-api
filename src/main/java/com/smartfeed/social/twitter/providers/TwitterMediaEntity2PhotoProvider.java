package com.smartfeed.social.twitter.providers;

import com.smartfeed.social.core.feed.Photo;
import org.springframework.social.twitter.api.MediaEntity;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 23:29
 */
public class TwitterMediaEntity2PhotoProvider implements Photo.PhotoDataProvider {
    MediaEntity mediaEntity;

    public TwitterMediaEntity2PhotoProvider(MediaEntity mediaEntity) {
        this.mediaEntity = mediaEntity;
    }

    @Override
    public String getExternalId() {
        return Long.toString(mediaEntity.getId());
    }

    @Override
    public String getPreviewUrl() {
        return mediaEntity.getUrl();
    }

    @Override
    public String getUrl() {
        return mediaEntity.getMediaUrl();
    }

    @Override
    public String getDescription() {
        return null;
    }
}
