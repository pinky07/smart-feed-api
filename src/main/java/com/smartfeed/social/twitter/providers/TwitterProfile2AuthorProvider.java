package com.smartfeed.social.twitter.providers;

import com.smartfeed.social.core.feed.Author;
import org.springframework.social.twitter.api.TwitterProfile;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 23:35
 */
public class TwitterProfile2AuthorProvider implements Author.AuthorDataProvider {
    TwitterProfile twitterProfile;

    public TwitterProfile2AuthorProvider(TwitterProfile twitterProfile) {
        this.twitterProfile = twitterProfile;
    }

    @Override
    public String getExternalId() {
        return Long.toString(twitterProfile.getId());
    }

    @Override
    public String getUsername() {
        return twitterProfile.getScreenName();
    }

    @Override
    public String getFullName() {
        return twitterProfile.getName();
    }

    @Override
    public String getAvatarUrl() {
        return twitterProfile.getProfileImageUrl();
    }
}
