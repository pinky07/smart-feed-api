package com.smartfeed.social.twitter.providers;

import com.smartfeed.social.SocialNetwork;
import com.smartfeed.social.core.feed.Author;
import com.smartfeed.social.core.feed.Photo;
import com.smartfeed.social.core.feed.Post;
import org.springframework.social.twitter.api.Entities;
import org.springframework.social.twitter.api.MediaEntity;
import org.springframework.social.twitter.api.Tweet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 23:23
 */
public class Tweet2PostProvider implements Post.PostProvider {
    private Tweet tweet;

    public Tweet2PostProvider(Tweet tweet) {
        this.tweet = tweet;
    }

    @Override
    public String getExternalId() {
        return Long.toString(tweet.getId());
    }

    @Override
    public String getText() {
        return tweet.getText();
    }

    @Override
    public Date getCreatedDate() {
        return tweet.getCreatedAt();
    }

    @Override
    public int getCommentsCount() {
        return 0;
    }

    @Override
    public int getFavesCount() {
        return tweet.getFavoriteCount();
    }

    @Override
    public int getRepostsCount() {
        return tweet.getRetweetCount();
    }

    @Override
    public List<Photo> getPhotos() {
        List<Photo> photos = new ArrayList<Photo>();
        Entities entities = tweet.getEntities();
        if(entities != null) {
            for(MediaEntity mediaEntity : entities.getMedia()) {
                photos.add(new Photo(new TwitterMediaEntity2PhotoProvider(mediaEntity)));
            }
        }


        return photos;
    }

    @Override
    public boolean canFave() {
        return true;
    }

    @Override
    public boolean canComment() {
        return true;
    }

    @Override
    public boolean canRepost() {
        return true;
    }

    @Override
    public boolean isFaved() {
        return tweet.isFavorited();
    }

    @Override
    public Author getAuthor() {
        return new Author(new TwitterProfile2AuthorProvider(tweet.getUser()));
    }

    @Override
    public boolean isRepost() {
        return tweet.isRetweet();
    }

    @Override
    public Post getRepostedPost() {
        Tweet retweet = tweet.getRetweetedStatus();
        if(retweet != null) {
            new Post(new Tweet2PostProvider(retweet));
        }

        return null;
    }

    @Override
    public String getProviderId() {
        return SocialNetwork.twitter.getProviderId();
    }
}
