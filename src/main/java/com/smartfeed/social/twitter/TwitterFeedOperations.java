package com.smartfeed.social.twitter;

import com.smartfeed.social.core.feed.AbstractFeedOperations;
import com.smartfeed.social.core.feed.Post;
import com.smartfeed.social.twitter.providers.Tweet2PostProvider;
import org.springframework.social.twitter.api.TimelineOperations;
import org.springframework.social.twitter.api.Tweet;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 23:19
 */
public class TwitterFeedOperations extends AbstractFeedOperations<Tweet> {
    private TimelineOperations timelineOperations;

    public TwitterFeedOperations(TimelineOperations timelineOperations) {
        this.timelineOperations = timelineOperations;
    }

    @Override
    protected Post convert(Tweet post) {
        return new Post(new Tweet2PostProvider(post));
    }

    @Override
    protected List<Tweet> loadHomeFeed(int size) {
        return timelineOperations.getHomeTimeline(size);
    }

    @Override
    protected List<Tweet> loadHomeFeed(Post after, int size) {
        List<Tweet> tweets = timelineOperations.getHomeTimeline(size + 1, 0,
                Long.parseLong(after.getExternalId()));
        return excludeEnabledPost(tweets);
    }

    @Override
    protected List<Tweet> loadUserFeed(int size) {
        return timelineOperations.getUserTimeline(size);
    }

    @Override
    protected List<Tweet> loadUserFeed(Post after, int size) {
        List<Tweet> tweets = timelineOperations.getUserTimeline(size + 1, 0,
                Long.parseLong(after.getExternalId()));
        return excludeEnabledPost(tweets);
    }

    private List<Tweet> excludeEnabledPost(List<Tweet> tweets) {
        if(!tweets.isEmpty()) {
            tweets.remove(0);
            ((ArrayList<?>)tweets).trimToSize();
        }

        return tweets;
    }
}
