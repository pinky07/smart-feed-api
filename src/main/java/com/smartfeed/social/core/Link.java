package com.smartfeed.social.core;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 22.11.14
 * Time: 21:03
 */
public class Link {
    @Field("url")
    private String url;
    @Field("capture")
    private String capture;
    @Field("title")
    private String title;
    @Field("description")
    private String description;

    public Link(LinkDataProvider linkDataProvider) {
        setUrl(linkDataProvider.getUrl());
        setCapture(linkDataProvider.getCapture());
        setTitle(linkDataProvider.getTitle());
        setDescription(linkDataProvider.getDescription());
    }

    public Link() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCapture() {
        return capture;
    }

    public void setCapture(String capture) {
        this.capture = capture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static interface LinkDataProvider {
        public String getUrl();
        public String getCapture();
        public String getTitle();
        public String getDescription();
    }
}
