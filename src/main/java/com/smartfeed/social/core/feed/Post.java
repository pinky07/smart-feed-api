package com.smartfeed.social.core.feed;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartfeed.core.persistence.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Author: Khamidullin Kamil
 * Date: 27.10.14
 * Time: 9:28
 */
@Document(collection = "posts")
public class Post extends BaseEntity implements Comparable<Post> {
    @Field("external_id")
    protected String externalId;
    @Field("original_time_created")
    protected Date originalTimeCreated;
    @Field("text")
    protected String text;
    @Field("author")
    protected Author author;
    @JsonProperty("is_repost")
    @Field("is_repost")
    protected boolean isRepost;
    @Field("reposted_post")
    protected Post repostedPost;
    @Field("photos")
    protected List<Photo> photos = new ArrayList<Photo>();

    //faves
    @Field("faves_count")
    protected int favesCount = -1;
    @Field("can_fave")
    protected boolean canFave = false;
    @Field("is_faved")
    protected boolean isFaved;

    //reposts
    @Field("repost_count")
    protected int repostsCount = -1;
    @Field("can_repost")
    protected boolean canRepost = false;

    //comments
    @Field("comment_count")
    protected int commentsCount = -1;
    @Field("can_comment")
    protected boolean canComment = false;

    @Field("provider_id")
    protected String providerId;

    @Field("user_id")
    protected String userId;

    @Field("meta")
    protected Meta meta;

    public Post() {
    }

    public String getProviderId() {
        return providerId;
    }

    public Author getAuthor() {
        return author;
    }

    public int compareTo(Post another) {
        Assert.notNull(another);
        Date anotherCreatedDate = another.getOriginalTimeCreated();

        return anotherCreatedDate.compareTo(originalTimeCreated);
    }

    public void setCreatedTime(Date date) {
        this.originalTimeCreated = date;
    }

    public Date getOriginalTimeCreated() {
        return this.originalTimeCreated;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public boolean isRepost() {
        return isRepost;
    }

    public void setRepost(boolean isRepost) {
        this.isRepost = isRepost;
    }

    public Post getRepostedPost() {
        return repostedPost;
    }

    public void setRepostedPost(Post repostedPost) {
        this.repostedPost = repostedPost;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public int getFavesCount() {
        return favesCount;
    }

    public void setFavesCount(int favesCount) {
        this.favesCount = favesCount;
    }

    public boolean isCanFave() {
        return canFave;
    }

    public void setCanFave(boolean canFave) {
        this.canFave = canFave;
    }

    public boolean isFaved() {
        return isFaved;
    }

    public void setFaved(boolean isFaved) {
        this.isFaved = isFaved;
    }

    public int getRepostsCount() {
        return repostsCount;
    }

    public void setRepostsCount(int repostsCount) {
        this.repostsCount = repostsCount;
    }

    public boolean isCanRepost() {
        return canRepost;
    }

    public void setCanRepost(boolean canRepost) {
        this.canRepost = canRepost;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public boolean isCanComment() {
        return canComment;
    }

    public void setCanComment(boolean canComment) {
        this.canComment = canComment;
    }


    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getNextExternalId() {
        return getMeta().getNextExternalId();
    }

    public String getPrevExternalId() {
        return getMeta().getPrevExternalId();
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Meta getMeta() {
        if(meta == null)
            meta = new Meta();

        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Post post = (Post) o;

        if (externalId != null ? !externalId.equals(post.externalId) : post.externalId != null) return false;
        if (originalTimeCreated != null ? !originalTimeCreated.equals(post.originalTimeCreated) : post.originalTimeCreated != null)
            return false;
        if (providerId != null ? !providerId.equals(post.providerId) : post.providerId != null) return false;
        if (userId != null ? !userId.equals(post.userId) : post.userId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (externalId != null ? externalId.hashCode() : 0);
        result = 31 * result + (originalTimeCreated != null ? originalTimeCreated.hashCode() : 0);
        result = 31 * result + (providerId != null ? providerId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Post{" +
                "providerId=" + providerId +
                ", externalId='" + externalId + '\'' +
                ", originalTimeCreated=" + originalTimeCreated +
                '}';
    }

    public Post(PostProvider dataProvider) {
        super(UUID.nameUUIDFromBytes((dataProvider.getExternalId() + dataProvider.getProviderId()).getBytes()));
        setExternalId(dataProvider.getExternalId());
        setCreatedTime(dataProvider.getCreatedDate());
        setText(dataProvider.getText());
        setFaved(dataProvider.isFaved());
        setFavesCount(dataProvider.getFavesCount());
        setCanFave(dataProvider.canFave());
        setCommentsCount(dataProvider.getCommentsCount());
        setCanComment(dataProvider.canComment());
        setRepostsCount(dataProvider.getRepostsCount());
        setCanRepost(dataProvider.canRepost());
        setAuthor(dataProvider.getAuthor());
        setPhotos(dataProvider.getPhotos());
        setRepostedPost(dataProvider.getRepostedPost());
        setRepost(dataProvider.isRepost());
        setProviderId(dataProvider.getProviderId());
    }

    public interface PostProvider {
        public String getExternalId();
        public String getText();
        public Date getCreatedDate();
        public int getCommentsCount();
        public int getFavesCount();
        public int getRepostsCount();
        public List<Photo> getPhotos();
        public boolean canFave();
        public boolean canComment();
        public boolean canRepost();
        public boolean isFaved();
        public Author getAuthor();
        public boolean isRepost();
        public Post getRepostedPost();
        public String getProviderId();
    }
}