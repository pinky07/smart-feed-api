package com.smartfeed.social.core.feed;

import org.springframework.social.RateLimitExceededException;

import java.util.*;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:04
 */
abstract public class AbstractFeedOperations<T> implements FeedOperations {

    protected abstract Post convert(T post);

    /**
     * Загружает все новые посты
     * @param size
     * @return
     */
    protected abstract List<T> loadHomeFeed(int size);

    /**
     * Загружает все посты после текущего
     * @param after
     * @param size
     * @return
     */
    protected abstract List<T> loadHomeFeed(Post after, int size);

    /**
     * Загружает все новые посты
     * @param size
     * @return
     */
    protected abstract List<T> loadUserFeed(int size);

    /**
     * Загружает все посты после текущего
     * @param after
     * @param size
     * @return
     */
    protected abstract List<T> loadUserFeed(Post after, int size);


    private List<Post> convertFeed(List<T> feed) {
        List<Post> postList = new ArrayList<Post>();
        for(T post : feed) {
            postList.add(convert(post));
        }

        return postList;
    }

    /**
     * Связывание постов между собой, каждый пост хранит в себе информацию о следующем и предыдущем посте,
     * необходимо для подгрузки следующей коллекции постов после любого поста
     * @param postList
     * @return
     */
    private List<Post> linkFeed(List<Post> postList) {
        if(postList.size() == 1) {
            Post lastPost = postList.get(0);
            if(lastPost.getMeta().getNextExternalId() != null) {
                lastPost.getMeta().setPrevExternalId("NONE");
            }
        }
        // Сортровка по умолчанию от более нового к более старому по времени
        NavigableSet<Post> navigableSet = new TreeSet<>(postList);

        for(Post post : navigableSet) {
            // Получем следующий пост от текущего поста в коллекции (более старый относительно текущего поста)
            Post prevPost = navigableSet.higher(post);
            // если он есть связываем его с текущим
            if(prevPost != null) {
                //устанавливаем как предыдущий пост по времени от текущего
                post.getMeta().setPrevExternalId(prevPost.getExternalId());
            }
            // Получаем предыдущий пост от текущего (более новый относительно текущего поста)
            Post nextPost = navigableSet.lower(post);
            // если он есть связываем его с текущим
            if(nextPost != null) {
                // устанавливаем как следующий пост по времени от текущего
                post.getMeta().setNextExternalId(nextPost.getExternalId());
            }
        }

        return new ArrayList<>(navigableSet);
    }

    /**
     * @return
     */
    public List<Post> getHomeFeed(int size) {
        return linkFeed(convertFeed(loadHomeFeed(size)));
    }

    /**
     *
     * @param afterPost
     * @return
     */
    public List<Post> getHomeFeed(Post afterPost, int size) {
        List<Post> postList = new ArrayList<>();
        postList.addAll(convertFeed(loadHomeFeed(afterPost, size)));
        postList.add(afterPost);

        return linkFeed(postList);
    }

    @Override
    public List<Post> getUserFeed(int size) {
        return linkFeed(convertFeed(loadUserFeed(size)));
    }

    @Override
    public List<Post> getUserFeed(Post afterPost, int size) {
        List<Post> postList = new ArrayList<Post>();
        postList.addAll(convertFeed(loadUserFeed(afterPost, size)));
        postList.add(afterPost);

        return linkFeed(postList);
    }

    @Override
    public List<Post> getFavoritesFeed(int size) {
        return null;
    }

    @Override
    public List<Post> getFavoritesFeed(Post afterPost, int size) {
        return null;
    }
}