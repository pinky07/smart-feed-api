package com.smartfeed.social.core.feed;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 05.11.14
 * Time: 20:53
 */
public interface FeedOperations {
    /**
     * Получение постов из домашней новостной ленты
     * @param size - количество постов
     * @return
     */
    public List<Post> getHomeFeed(int size);

    /**
     * Получение постов из домашней ленты после конкретного поста
     * @param afterPost - пост после которой следуют получить ленту
     * @param size
     * @return
     */
    public List<Post> getHomeFeed(Post afterPost, int size);

    /**
     * Получение получение ленты постов опубликованных текущим пользователем
     * @param size
     * @return
     */
    public List<Post> getUserFeed(int size);
    public List<Post> getUserFeed(Post afterPost, int size);

    /**
     * Получение избранных постов пользователя
     * @param size
     * @return
     */
    public List<Post> getFavoritesFeed(int size);
    public List<Post> getFavoritesFeed(Post afterPost, int size);
}
