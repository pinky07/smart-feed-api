package com.smartfeed.social.core.feed;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 27.10.14
 * Time: 9:43
 */
public class Photo {
    @Field("external_id")
    protected String externalId;
    @Field("preview_url")
    protected String previewUrl;
    @Field("url")
    protected String url;
    @Field("description")
    protected String description;

    public Photo() {
    }

    public Photo(PhotoDataProvider dataProvider){
        setExternalId(dataProvider.getExternalId());
        setPreviewUrl(dataProvider.getPreviewUrl());
        setDescription(dataProvider.getPreviewUrl());
        setUrl(dataProvider.getUrl());
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getUrl() {
        return url;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static interface PhotoDataProvider {
        public String getExternalId();
        public String getPreviewUrl();
        public String getDescription();
        public String getUrl();
    }
}