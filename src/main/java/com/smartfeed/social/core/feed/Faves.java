package com.smartfeed.social.core.feed;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 24.11.14
 * Time: 21:49
 */
public class Faves {
    //faves
    @Field("faves_count")
    protected int favesCount = -1;
    @Field("can_fave")
    protected boolean canFave;
    @Field("is_faved")
    protected boolean isFaved;

    public int getFavesCount() {
        return favesCount;
    }

    public void setFavesCount(int favesCount) {
        this.favesCount = favesCount;
    }

    public boolean isCanFave() {
        return canFave;
    }

    public void setCanFave(boolean canFave) {
        this.canFave = canFave;
    }

    public boolean isFaved() {
        return isFaved;
    }

    public void setFaved(boolean isFaved) {
        this.isFaved = isFaved;
    }
}
