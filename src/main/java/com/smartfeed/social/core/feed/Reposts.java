package com.smartfeed.social.core.feed;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 24.11.14
 * Time: 22:03
 */
public class Reposts {
    @Field("repost_count")
    protected int repostsCount = -1;
    @Field("can_repost")
    protected boolean canRepost = false;

    public int getRepostsCount() {
        return repostsCount;
    }

    public void setRepostsCount(int repostsCount) {
        this.repostsCount = repostsCount;
    }

    public boolean isCanRepost() {
        return canRepost;
    }

    public void setCanRepost(boolean canRepost) {
        this.canRepost = canRepost;
    }
}
