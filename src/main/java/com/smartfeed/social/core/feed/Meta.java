package com.smartfeed.social.core.feed;

import org.springframework.data.mongodb.core.mapping.Field;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Khamidullin Kamil
 * Date: 24.11.14
 * Time: 19:03
 */
public class Meta {
    @Field("prev_external_id")
    protected String prevExternalId;
    @Field("next_external_id")
    protected String nextExternalId;
    @Field("params")
    protected Map<String, Object> params = new HashMap<>();

    public String getPrevExternalId() {
        return prevExternalId;
    }

    public void setPrevExternalId(String prevExternalId) {
        this.prevExternalId = prevExternalId;
    }

    public String getNextExternalId() {
        return nextExternalId;
    }

    public void setNextExternalId(String nextExternalId) {
        this.nextExternalId = nextExternalId;
    }

    public void addParam(String key, String values) {
        this.params.put(key, values);
    }

    public boolean hasPagingParam(String key) {
        return this.params.containsKey(key);
    }

    public String getParam(String key) {
        return (String)this.params.get(key);
    }
}