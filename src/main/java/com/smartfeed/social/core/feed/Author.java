package com.smartfeed.social.core.feed;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 27.10.14
 * Time: 9:44
 */
public class Author {
    @Field("external_id")
    private String externalId;
    @Field("username")
    private String username;
    @Field("full_name")
    private String fullName;
    @Field("avatar_url")
    private String avatarUrl;

    public Author() {
    }

    public Author(AuthorDataProvider dataProvider) {
        setExternalId(dataProvider.getExternalId());
        setUsername(dataProvider.getUsername());
        setFullName(dataProvider.getFullName());
        setAvatarUrl(dataProvider.getAvatarUrl());
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public static interface AuthorDataProvider {
        public String getExternalId();
        public String getUsername();
        public String getFullName();
        public String getAvatarUrl();
    }
}