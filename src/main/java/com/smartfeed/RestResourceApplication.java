package com.smartfeed;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.smartfeed.core.exception.AccessDeniedExceptionMapper;
import com.smartfeed.core.exception.ProviderAPIExceptionMapper;
import com.smartfeed.core.filter.jersey.JerseyCrossOriginResourceSharingFilter;
import com.smartfeed.core.resource.GenericExceptionMapper;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.ws.rs.container.ContainerResponseFilter;

import static com.fasterxml.jackson.databind.PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES;

public class RestResourceApplication extends ResourceConfig {

    public final static String API_VERSION = "v1.0";
    public final static String AUTH_SERVLET_URL = "/" + API_VERSION + "/auth";

    public RestResourceApplication() {

        packages("com.smartfeed.core.resource", "com.smartfeed.endpoints.user.resource", "com.smartfeed.endpoints.connection.resource",
        "com.smartfeed.sample", "com.smartfeed.endpoints.feed.resource", "com.smartfeed.endpoints.connection");

        register(RequestContextFilter.class);

        ApplicationContext rootCtx = ContextLoader.getCurrentWebApplicationContext();
        ContainerResponseFilter filter = rootCtx.getBean(JerseyCrossOriginResourceSharingFilter.class);
        register(filter);

        register(GenericExceptionMapper.class);
        register(AccessDeniedExceptionMapper.class);
        register(ProviderAPIExceptionMapper.class);

        register(JacksonFeature.class);

        // create custom ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        // create JsonProvider to provide custom ObjectMapper
        JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
        provider.setMapper(mapper);

        register(provider);
    }
}