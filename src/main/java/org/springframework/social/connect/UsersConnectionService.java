package org.springframework.social.connect;

import com.smartfeed.endpoints.connection.ConnectionStatusDao;
import net.exacode.spring.social.connect.GenericUsersConnectionRepository;
import net.exacode.spring.social.connect.SocialConnectionDao;

/**
 * Author: Khamidullin Kamil
 * Date: 07.12.14
 * Time: 21:15
 */
public class UsersConnectionService extends GenericUsersConnectionRepository {
    private final SocialConnectionDao mongoService;
    private final ConnectionFactoryLocator connectionFactoryLocator;
    private final ConnectionStatusDao connectionStatusDao;

    public UsersConnectionService(SocialConnectionDao mongoService, ConnectionStatusDao connectionStatusDao, ConnectionFactoryLocator connectionFactoryLocator) {
        super(mongoService, connectionFactoryLocator);
        this.mongoService = mongoService;
        this.connectionStatusDao = connectionStatusDao;
        this.connectionFactoryLocator = connectionFactoryLocator;
    }

    @Override
    public ConnectionRepository createConnectionRepository(String userId) {
        return new ConnectionService(userId, mongoService, connectionStatusDao, connectionFactoryLocator);
    }
}
