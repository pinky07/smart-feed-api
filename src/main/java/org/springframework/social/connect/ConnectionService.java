package org.springframework.social.connect;

import com.smartfeed.endpoints.connection.ConnectionStatus;
import com.smartfeed.endpoints.connection.ConnectionStatusDao;
import net.exacode.spring.social.connect.GenericConnectionRepository;
import net.exacode.spring.social.connect.SocialConnectionDao;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Author: Khamidullin Kamil
 * Date: 07.12.14
 * Time: 21:16
 */
public class ConnectionService extends GenericConnectionRepository {
    private final SocialConnectionDao connectionService;
    private final String userId;
    private final ConnectionStatusDao connectionStatusDao;


    public ConnectionService(String userId, SocialConnectionDao connectionService, ConnectionStatusDao connectionStatusDao, ConnectionFactoryLocator connectionFactoryLocator) {
        super(userId, connectionService, connectionFactoryLocator);
        this.userId = userId;
        this.connectionService = connectionService;
        this.connectionStatusDao = connectionStatusDao;
    }

    @Transactional
    @Override
    public void addConnection(Connection<?> connection) {
        try {
            ConnectionData data = connection.createData();
            String providerId = data.getProviderId();

            int rank = connectionService.getMaxRank(userId, providerId);
            connectionService.create(userId, connection, rank);

            ConnectionStatus connectionStatus = connectionStatusDao.findByUserIdAndProviderId(userId, providerId);
            if(connectionStatus == null) {
                connectionStatus = new ConnectionStatus();
                connectionStatus.setUserId(userId);
                connectionStatus.setProviderId(providerId);
            }

            connectionStatus.setEnable(true);
            connectionStatusDao.save(connectionStatus);
        } catch (DuplicateKeyException e) {
            throw new DuplicateConnectionException(connection.getKey());
        }
    }

    @Transactional
    @Override
    public void removeConnections(String providerId) {
        super.removeConnections(providerId);
        ConnectionStatus connectionStatus = connectionStatusDao.findByUserIdAndProviderId(userId, providerId);
        if(connectionStatus != null) {
            connectionStatusDao.delete(connectionStatus);
        }
    }

    @Transactional
    @Override
    public void removeConnection(ConnectionKey connectionKey) {
        super.removeConnection(connectionKey);
        ConnectionStatus connectionStatus = connectionStatusDao.findByUserIdAndProviderId(userId, connectionKey.getProviderId());
        if(connectionStatus != null) {
            connectionStatusDao.delete(connectionStatus);
        }
    }
}
