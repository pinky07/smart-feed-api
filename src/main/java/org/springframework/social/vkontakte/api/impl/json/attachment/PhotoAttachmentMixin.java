package org.springframework.social.vkontakte.api.impl.json.attachment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 15:17
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoAttachmentMixin {
    @JsonProperty("id")
    private String photoId;
    @JsonProperty("owner_id")
    private String ownerId;
    @JsonProperty("photo_130")
    private String src;
    @JsonProperty("photo_604")
    private String srcBig;
}