package org.springframework.social.vkontakte.api.impl.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.social.vkontakte.api.VKontakteDate;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 18:19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfileMixin {
    @JsonCreator
    UserProfileMixin(@JsonProperty("id") String id,
                     @JsonProperty("first_name") String firstName,
                     @JsonProperty("last_name") String lastName,
                     @JsonProperty("screen_name") String screnName,
                     @JsonProperty("sex") @JsonDeserialize(using = VKGenderDeserializer.class) String gender,
                     @JsonProperty("photo_100") String photo,
                     @JsonProperty("photo_50") String photoSmall) {
    }

    static class VKGenderDeserializer extends JsonDeserializer<String> {
        @Override
        public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String genderValue = jp.getText();
            if(genderValue.equals("1")) {
                return "W";
            } else if(genderValue.equals("2")){
                return "M";
            }
            return null;
        }

    }
}
