package org.springframework.social.vkontakte.api;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:20
 */
public interface VK extends VKontakte {
    IFeedOperations getFeedOperations();
}
