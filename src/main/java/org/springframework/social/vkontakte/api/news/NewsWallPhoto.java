package org.springframework.social.vkontakte.api.news;

import org.springframework.social.vkontakte.api.attachment.PhotoAttachment;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 13:28
 */
public class NewsWallPhoto extends NewsItem {
    Photos photos;

    public Photos getPhotos() {
        return photos;
    }
}
