package org.springframework.social.vkontakte.api.impl.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 16:48
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostCommentsMixin {

    @JsonCreator
    PostCommentsMixin(@JsonProperty("count") int count) {}

    @JsonProperty("count")
    private int count;

    @JsonProperty("can_post")
    private boolean canPost;

}
