package org.springframework.social.vkontakte.api.impl.json.attachment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.social.vkontakte.api.attachment.Attachment;

/**
 * Author: Khamidullin Kamil
 * Date: 15.12.14
 * Time: 22:42
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotosListAttachmentMixin {
}
