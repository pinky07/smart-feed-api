package org.springframework.social.vkontakte.api.attachment;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 16:29
 */
public enum AttachmentType {
    ALBUM, PHOTO, POSTED_PHOTO, PHOTOS_LIST, VIDEO, AUDIO, DOC, GRAFFITI, LINK, NOTE, APPLICATION, POLL, PAGE
}
