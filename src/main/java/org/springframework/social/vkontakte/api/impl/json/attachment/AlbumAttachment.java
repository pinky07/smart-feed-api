package org.springframework.social.vkontakte.api.impl.json.attachment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.vkontakte.api.attachment.Attachment;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 16:26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlbumAttachment extends Attachment {
    @JsonProperty("id")
    private String id;
    @JsonProperty("owner_id")
    private String ownerId;
}
