package org.springframework.social.vkontakte.api.impl.json;

import org.springframework.social.vkontakte.api.Group;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.UserProfile;
import org.springframework.social.vkontakte.api.attachment.Photo;
import org.springframework.social.vkontakte.api.attachment.PhotosListAttachment;
import org.springframework.social.vkontakte.api.impl.json.attachment.PhotoAttachmentMixin;
import org.springframework.social.vkontakte.api.impl.json.attachment.PhotosListAttachmentMixin;
import org.springframework.social.vkontakte.api.impl.json.news.NewsItemMixin;
import org.springframework.social.vkontakte.api.impl.json.news.NewsPhotoMixin;
import org.springframework.social.vkontakte.api.impl.json.news.NewsPostMixin;
import org.springframework.social.vkontakte.api.impl.json.news.PhotosMixin;
import org.springframework.social.vkontakte.api.news.*;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 3:48
 */
public class VKModule extends VKontakteModule {
    @Override
    public void setupModule(SetupContext context) {
        super.setupModule(context);
        context.setMixInAnnotations(NewsItem.class, NewsItemMixin.class);
        context.setMixInAnnotations(NewsPost.class, NewsPostMixin.class);
        context.setMixInAnnotations(NewsPhoto.class, NewsPhotoMixin.class);
        context.setMixInAnnotations(Post.class, PostMixin.class);
        context.setMixInAnnotations(NewsWallPhoto.class, NewsPhotoMixin.class);
        context.setMixInAnnotations(Photos.class, PhotosMixin.class);
        context.setMixInAnnotations(Photo.class, PhotoAttachmentMixin.class);
        context.setMixInAnnotations(Post.Comments.class, PostCommentsMixin.class);
        context.setMixInAnnotations(Group.class, GroupMixin.class);
        context.setMixInAnnotations(UserProfile.class, UserProfileMixin.class);
        context.setMixInAnnotations(PhotosListAttachment.class, PhotosListAttachmentMixin.class);
    }
}
