package org.springframework.social.vkontakte.api.news;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 13:21
 */
public enum NewsItemType {
    POST, WALL_PHOTO, PHOTO
}
