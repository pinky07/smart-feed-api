package org.springframework.social.vkontakte.api;

import org.springframework.social.vkontakte.api.news.NewsItem;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:04
 */
public interface IFeedOperations {

    /**
     * Retrieves recent posts for the authenticated user.
     * Requires "read_stream" permission to read non-public posts.
     * Returns up to the most recent 25 posts.
     *
     * @return a list of {@link Post}s for the authenticated user.
     * @throws org.springframework.social.ApiException if there is an error while communicating with VKontakte.
     * @throws org.springframework.social.MissingAuthorizationException if VKontakteTemplate was not created with an access token.
     */
    List<NewsItem> getFeed(int size);

    /**
     * Retrieves recent posts for the authenticated user.
     * Requires "read_stream" permission to read non-public posts.
     *
     * @param startTime the offset into the feed to start retrieving posts.
     * @param endTime the maximum number of posts to return.
     * @return a list of {@link Post}s for the authenticated user.
     * @throws org.springframework.social.ApiException if there is an error while communicating with VKontakte.
     * @throws org.springframework.social.MissingAuthorizationException if VKontakteTemplate was not created with an access token.
     */
    List<NewsItem> getFeed(long startTime, long endTime, int size);

    /**
     *
     * @param startFrom
     * @param size
     * @return
     */
    List<NewsItem> getFeed(String startFrom, int size);

    /**
     *
     * @param pagingParameters
     * @return
     */
    List<NewsItem> getFeed(PagingParameters pagingParameters);

    /**
     * Retrieves recent feed entries for a given user.
     * Requires "read_stream" permission to read non-public posts.
     * Returns up to the most recent 25 posts.
     *
     * @param ownerId the Facebook ID or alias for the owner (user, group, event, page, etc) of the feed.
     * @param startTime the offset into the feed to start retrieving posts.
     * @param endTime the maximum number of posts to return.
     * @return a list of {@link Post}s for the specified user.
     * @throws org.springframework.social.ApiException if there is an error while communicating with VKontakte.
     * @throws org.springframework.social.MissingAuthorizationException if VKontakteTemplate was not created with an access token.
     */
    List<NewsItem> getFeed(String ownerId, String startFrom, long startTime, long endTime, int count);

    /**
     * Searches the authenticated user's feed.
     * Returns up to 25 posts that match the query.
     *
     * @param query the search query (e.g., "football")
     * @return a list of {@link Post}s that match the search query
     * @throws org.springframework.social.ApiException if there is an error while communicating with VKontakte.
     * @throws org.springframework.social.MissingAuthorizationException if VKontakteTemplate was not created with an access token.
     */
    List<NewsItem> searchUserFeed(String query);

    /**
     * Searches the authenticated user's feed.
     *
     * @param query the search query (e.g., "football")
     * @param startTime the offset into the feed to start retrieving posts.
     * @param endTime the maximum number of posts to return.
     * @return a list of {@link Post}s that match the search query
     * @throws org.springframework.social.ApiException if there is an error while communicating with VKontakte.
     * @throws org.springframework.social.MissingAuthorizationException if VKontakteTemplate was not created with an access token.
     */
    List<NewsItem> searchUserFeed(String query, long startTime, long endTime);
}