package org.springframework.social.vkontakte.api.impl.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.attachment.Attachment;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 15:55
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostMixin {
    @JsonCreator
    PostMixin(@JsonProperty("id") String postId, @JsonProperty("date") @JsonDeserialize(using = DateDeserializer.class) Date createdDate, @JsonProperty("text") String text, @JsonProperty("owner_id") String authorId) {
    }

    @JsonProperty("copy_history")
    List<Post> copyHistory;

    @JsonProperty("date")
    @JsonDeserialize(using = DateDeserializer.class)
    Date createdDate;

    @JsonProperty("id")
    String id;

    @JsonProperty("owner_id")
    String authorId;

    @JsonProperty("text")
    String text;

    @JsonProperty("likes")
    Post.Likes likes;

    @JsonProperty("reposts")
    Post.Reposts reposts;

    @JsonProperty("comments")
    Post.Comments comments;

    @JsonProperty("attachments")
    List<? extends Attachment> attachments;

    static class DateDeserializer extends JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            long dateValue = jp.getLongValue();
            return new Date(dateValue*1000);
        }
    }
}
