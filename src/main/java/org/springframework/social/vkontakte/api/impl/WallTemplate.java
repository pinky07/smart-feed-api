package org.springframework.social.vkontakte.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.smartfeed.social.core.feed.*;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.vkontakte.api.*;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.news.NewsItem;
import org.springframework.social.vkontakte.api.news.NewsPost;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 15:14
 */
public class WallTemplate extends AbstractVKontakteOperations implements IWallOperations {
    private final RestTemplate restTemplate;

    public WallTemplate(RestTemplate restTemplate, String accessToken, ObjectMapper objectMapper, boolean isAuthorizedForUser) {
        super(isAuthorizedForUser, accessToken, objectMapper);
        this.restTemplate = restTemplate;
    }

    private List<Post> deserializePostItems(VKGenericResponse response) {
        List<Post> newsItems = deserializeArray((ArrayNode) response.getResponse().get("items"), Post.class);
        List<UserProfile> profiles = deserializeArray((ArrayNode) response.getResponse().get("profiles"), UserProfile.class);
        List<Group> groups = deserializeArray((ArrayNode) response.getResponse().get("groups"), Group.class);
        String count = response.getResponse().get("count").asText();

        for(Post wallPost : newsItems) {
            wallPost.setAuthor(findAuthor(wallPost.getAuthorId(), groups, profiles));
            List<Post> copyHistory = wallPost.getCopyHistory();
            if(copyHistory != null) {
                for(Post post : copyHistory) {
                    post.setAuthor(findAuthor(post.getAuthorId(), groups, profiles));
                }
            }
        }

        return newsItems;
    }

    private Object findAuthor(String authorId, List<Group> groups, List<UserProfile> profiles) {
        if(authorId.charAt(0) == '-') {
            authorId = authorId.substring(1);
            for(Group group : groups) {
                if(group.getId().equals(authorId)) {
                    return group;
                }
            }
        } else {
            for(UserProfile profile : profiles) {
                if(profile.getId().equals(authorId)) {
                    return profile;
                }
            }
        }

        return null;
    }

    private <T> List<T> deserializeArray(ArrayNode node, Class<T> tClass) {
        List<T> list = new ArrayList<T>();
        for (int i = 0; i < node.size(); i++) {
            try {
                list.add(objectMapper.convertValue(node.get(i), tClass));
            } catch (Exception e) {
                throw new UncategorizedApiException("vkontakte", "Error deserializing: " + node.get(i), e);
            }
        }

        return list;
    }

    @Override
    public List<Post> getPosts() {
        return getPosts(-1, -1);
    }

    @Override
    public List<Post> getPosts(int offset, int limit) {
        return getPostsForUser(null, offset, limit);
    }

    public List<Post> getPostsForUser(String userId, int offset, int limit) {
        requireAuthorization();
        Properties props = new Properties();
        if (offset >= 0) {
            props.put("offset", offset);
        }
        if (limit > 0) {
            props.put("count", limit);
        }
        if (userId != null) {
            props.put("owner_id", userId);
        }

        props.put("extended", 1);

        URI uri = makeOperationURL("wall.get", props, ApiVersion.VERSION_5_21);
        VKGenericResponse response = restTemplate.getForObject(uri, VKGenericResponse.class);
        checkForError(response);

        List<Post> postList = deserializePostItems(response);

        if(!postList.isEmpty()) {
            Post lastPost = postList.get(postList.size() - 1);
            int padding = postList.size();
            if(offset > 0) {
                padding += offset;
            }

            lastPost.setPadding(padding);
        }


        return postList;
    }

    @Override
    public Post getPost(String userId, String postId) {
        requireAuthorization();
        Properties props = new Properties();
        props.put("posts", userId + "_" + postId);

        // http://vk.com/dev/wall.getById
        URI uri = makeOperationURL("wall.getById", props, ApiVersion.VERSION_5_21);
        VKGenericResponse response = restTemplate.getForObject(uri, VKGenericResponse.class);
        checkForError(response);
        try {
            return objectMapper.readValue(response.getResponse().get(0).toString(), Post.class);
        } catch (IOException e) {
            throw new UncategorizedApiException("vkontakte", "Error deserializing: " + response.getResponse(), e);
        }
    }

    @Override
    public PostStatus post(PostData postData) {
        requireAuthorization();

        // http://vk.com/dev/wall.post
        URI uri = makeOperationURL("wall.post", postData.toProperties(), ApiVersion.VERSION_3_0);
        PostStatusResponse response = restTemplate.getForObject(uri, PostStatusResponse.class);
        checkForError(response);

        return response.getStatus();
    }
}
