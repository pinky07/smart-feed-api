package org.springframework.social.vkontakte.api.impl.json.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.attachment.Attachment;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 13:54
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsPostMixin {
    @JsonProperty("text")
    String text;

    @JsonProperty("copy_history")
    List<Post> copyHistory;

    @JsonProperty("likes")
    Post.Likes likes;

    @JsonProperty("reposts")
    Post.Reposts reposts;

    @JsonProperty("comments")
    Post.Comments comments;

    @JsonProperty("attachments")
    List<? extends Attachment> attachments;
}
