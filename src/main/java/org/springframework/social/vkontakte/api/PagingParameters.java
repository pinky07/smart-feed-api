package org.springframework.social.vkontakte.api;

import java.io.Serializable;
import java.util.Properties;

/**
 * Author: Khamidullin Kamil
 * Date: 24.11.14
 * Time: 0:13
 */
public class PagingParameters implements Serializable {
    private String ownerId;
    private String startFrom;
    private long startTime;
    private long endTime;
    private int count;

    public PagingParameters(String ownerId, String startFrom, long startTime, long endTime, int count) {
        this.ownerId = ownerId;
        this.startFrom = startFrom;
        this.startTime = startTime;
        this.endTime = endTime;
        this.count = count;
    }

    public Properties toMap() {
        Properties props = new Properties();

        if (ownerId != null) {
            props.put("source_ids", ownerId);
        }

        if(startFrom != null) {
            props.put("start_from", startFrom);
        }

        if(startTime > 0) {
            props.put("start_time", startTime);
        }

        if(endTime > 0) {
            props.put("end_time", endTime);
        }

        if(count > 0) {
            props.put("count", count);
        }

        props.put("filters", "post,photo");

        return props;
    }
}
