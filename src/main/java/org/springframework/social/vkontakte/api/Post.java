package org.springframework.social.vkontakte.api;

import org.springframework.social.vkontakte.api.attachment.Attachment;
import org.springframework.social.vkontakte.api.news.NewsItem;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 15:59
 */
public class Post {
    private final String id;
    private final Date date;
    private final String text;
    private final String authorId;
    private Object author;
    List<Post> copyHistory;

    private Likes likes;
    private Reposts reposts;
    private Comments comments;
    private List<? extends Attachment> attachments;

    private int padding;

    public Post(String id, Date date, String text, String authorId) {
        this.id = id;
        this.date = date;
        this.text = text;
        this.authorId = authorId;
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public Likes getLikes() {
        return likes;
    }

    public Reposts getReposts() {
        return reposts;
    }

    public Comments getComments() {
        return comments;
    }

    public String getAuthorId() {
        return authorId;
    }

    public Object getAuthor() {
        return author;
    }

    public void setAuthor(Object author) {
        this.author = author;
    }

    public List<? extends Attachment> getAttachments() {
        return attachments;
    }

    public List<Post> getCopyHistory() {
        return copyHistory;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public static class Comments {
        private int count;
        private boolean canPost;

        public int getCount() {
            return count;
        }

        public boolean isCanPost() {
            return canPost;
        }

        @Override
        public String toString() {
            return "Comments{" +
                    "count=" + count +
                    ", canComment=" + canPost +
                    '}';
        }
    }

    public static class Likes {
        private int count;
        private boolean userLikes;
        private boolean canLike;
        private boolean canPublish;

        public int getCount() {
            return count;
        }

        public boolean isUserLikes() {
            return userLikes;
        }

        public boolean isCanLike() {
            return canLike;
        }

        public boolean isCanPublish() {
            return canPublish;
        }

        @Override
        public String toString() {
            return "Likes{" +
                    "count=" + count +
                    ", userLikes=" + userLikes +
                    ", canLike=" + canLike +
                    ", canPublish=" + canPublish +
                    '}';
        }
    }

    public static class Reposts {
        private int count;
        private boolean userReposted;

        public int getCount() {
            return count;
        }

        public boolean isUserReposted() {
            return userReposted;
        }

        @Override
        public String toString() {
            return "Reposts{" +
                    "count=" + count +
                    ", userReposted=" + userReposted +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", text='" + text + '\'' +
                ", likes=" + likes +
                ", reposts=" + reposts +
                ", attachments=" + (attachments == null ? null : StringUtils.collectionToDelimitedString(attachments, ",\n")) +
                '}';
    }
}
