package org.springframework.social.vkontakte.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.smartfeed.social.core.*;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.vkontakte.api.*;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.news.NewsItem;
import org.springframework.social.vkontakte.api.news.NewsPost;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:36
 */
public class FeedTemplate extends AbstractVKontakteOperations implements IFeedOperations {
    private final int DEFAULT_NUMBER_OF_POSTS = 25;

    private final RestTemplate restTemplate;

    public FeedTemplate(RestTemplate restTemplate, String accessToken, ObjectMapper objectMapper, boolean isAuthorizedForUser) {
        super(isAuthorizedForUser, accessToken, objectMapper);
        this.restTemplate = restTemplate;
    }

    private List<NewsItem> deserializeNewsItems(VKGenericResponse response) {
        List<NewsItem> newsItems = deserializeArray((ArrayNode) response.getResponse().get("items"), NewsItem.class);
        List<UserProfile> profiles = deserializeArray((ArrayNode) response.getResponse().get("profiles"), UserProfile.class);
        List<Group> groups = deserializeArray((ArrayNode) response.getResponse().get("groups"), Group.class);
        String from = response.getResponse().get("next_from").asText();

        for(NewsItem newsItem : newsItems) {
            newsItem.setAuthor(findAuthor(newsItem.getAuthorId(), groups, profiles));
            if(newsItem instanceof NewsPost) {
                List<Post> copyHistory = ((NewsPost)newsItem).getCopyHistory();
                if(copyHistory != null) {
                    for(Post post : copyHistory) {
                        post.setAuthor(findAuthor(post.getAuthorId(), groups, profiles));
                    }
                }
            }
        }

        if(!newsItems.isEmpty()) {
            NewsItem lastPost = newsItems.get(newsItems.size() - 1);
            lastPost.setFrom(from);
        }

        return newsItems;
    }

    private Object findAuthor(String authorId, List<Group> groups, List<UserProfile> profiles) {
        if(authorId.charAt(0) == '-') {
            authorId = authorId.substring(1);
            for(Group group : groups) {
                if(group.getId().equals(authorId)) {
                    return group;
                }
            }
        } else {
            for(UserProfile profile : profiles) {
                if(profile.getId().equals(authorId)) {
                    return profile;
                }
            }
        }

        return null;
    }

    private <T> List<T> deserializeArray(ArrayNode node, Class<T> tClass) {
        List<T> list = new ArrayList<T>();
        for (int i = 0; i < node.size(); i++) {
            try {
                list.add(objectMapper.convertValue(node.get(i), tClass));
            } catch (Exception e) {
                throw new UncategorizedApiException("vkontakte", "Error deserializing: " + node.get(i), e);
            }
        }

        return list;
    }

    @Override
    public List<NewsItem> getFeed(int count) {
        return getFeed(null, null, 0, 0, count);
    }

    @Override
    public List<NewsItem> getFeed(long startTime, long endTime, int count) {
        return getFeed(new PagingParameters(null, null, startTime, endTime, count));
    }

    @Override
    public List<NewsItem> getFeed(String startFrom, int size) {
        return getFeed(new PagingParameters(null, startFrom, 0, 0, size));
    }

    @Override
    public List<NewsItem> getFeed(PagingParameters pagingParameters) {
        requireAuthorization();

        URI uri = makeOperationURL("newsfeed.get", pagingParameters.toMap(), ApiVersion.VERSION_5_21);
        VKGenericResponse response = restTemplate.getForObject(uri, VKGenericResponse.class);
            checkForError(response);

        return deserializeNewsItems(response);
    }

    @Override
    public List<NewsItem> getFeed(String uid, String startFrom, long startTime, long endTime, int count) {
        return getFeed(new PagingParameters(uid, startFrom, startTime, endTime, count));
    }

    @Override
    public List<NewsItem> searchUserFeed(String query) {
        return searchUserFeed(query, 0, DEFAULT_NUMBER_OF_POSTS);
    }

    @Override
    public List<NewsItem> searchUserFeed(String query, long startTime, long endTime) {
        requireAuthorization();
        Properties props = new Properties();
        props.put("q", query);
        if(startTime > 0) {
            props.put("start_time", startTime);
        }

        if(endTime > 0) {
            props.put("end_time", endTime);
        }

        URI uri = makeOperationURL("newsfeed.search", props, ApiVersion.VERSION_5_21);
        VKGenericResponse response = restTemplate.getForObject(uri, VKGenericResponse.class);
        checkForError(response);

        return deserializeNewsItems(response);
    }
}