package org.springframework.social.vkontakte.api.news;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.vkontakte.api.Post;
import org.springframework.social.vkontakte.api.attachment.Attachment;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 13:26
 */
public class NewsPost extends NewsItem {
    private String text;
    private List<? extends Attachment> attachments;
    List<Post> copyHistory;

    Post.Likes likes;
    Post.Reposts reposts;
    Post.Comments comments;

    public String getText() {
        return text;
    }

    public List<? extends Attachment> getAttachments() {
        return attachments;
    }

    public List<Post> getCopyHistory() {
        return copyHistory;
    }

    public Post.Likes getLikes() {
        return likes;
    }

    public Post.Reposts getReposts() {
        return reposts;
    }

    public Post.Comments getComments() {
        return comments;
    }
}
