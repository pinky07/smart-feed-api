package org.springframework.social.vkontakte.api.impl.json.attachment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.social.vkontakte.api.attachment.*;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 16:30
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(name="album", value=AlbumAttachment.class),
        @JsonSubTypes.Type(name="link", value=LinkAttachment.class),
        @JsonSubTypes.Type(name="photo", value=PhotoAttachment.class),
        @JsonSubTypes.Type(name="posted_photo", value=PhotoAttachment.class),
        @JsonSubTypes.Type(name="video", value=VideoAttachment.class),
        @JsonSubTypes.Type(name="audio", value=AudioAttachment.class),
        @JsonSubTypes.Type(name="graffiti", value=GraffitiAttachment.class),
        @JsonSubTypes.Type(name="doc", value=DocumentAttachment.class),
        @JsonSubTypes.Type(name="note", value=NoteAttachment.class),
        @JsonSubTypes.Type(name="app", value=ApplicationAttachment.class),
        @JsonSubTypes.Type(name="poll", value=PollAttachment.class),
        @JsonSubTypes.Type(name="page", value=PageAttachment.class),
        @JsonSubTypes.Type(name="photos_list", value=PhotosListAttachment.class)
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachmentMixin {

    @JsonProperty("type")
    @JsonDeserialize(using = AttachmentTypeDeserializer.class)
    AttachmentType type;

    private static class AttachmentTypeDeserializer extends JsonDeserializer<AttachmentType> {
        @Override
        public AttachmentType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return AttachmentType.valueOf(jp.getText().toUpperCase());
        }
    }
}