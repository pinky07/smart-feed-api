package org.springframework.social.vkontakte.api;

import org.springframework.social.vkontakte.api.news.NewsItem;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 4:07
 */
public class VKFeed extends VKResponse {
    private List<NewsItem> newsPosts;

    public VKFeed(List<NewsItem> newsPosts) {
        this.newsPosts = newsPosts;
    }
}
