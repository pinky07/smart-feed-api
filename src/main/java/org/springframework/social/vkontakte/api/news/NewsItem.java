package org.springframework.social.vkontakte.api.news;

import org.springframework.social.vkontakte.api.VKontakteProfile;

import java.util.Date;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:38
 */
public class NewsItem {
    private String id;
    private Date createdDate;
    private String authorId;
    private Object author;
    private NewsItemType type;
    private String from;

    public String getId() {
        return id;
    }

    public void setAuthor(Object author) {
        this.author = author;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getAuthorId() {
        return authorId;
    }

    public Object getAuthor() {
        return author;
    }

    public NewsItemType getType() {
        return type;
    }
}