package org.springframework.social.vkontakte.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.vkontakte.api.*;
import org.springframework.social.vkontakte.api.impl.*;
import org.springframework.social.vkontakte.api.impl.json.VKModule;
import org.springframework.social.vkontakte.api.impl.json.VKontakteModule;

import java.util.LinkedList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:19
 */
public class VKTemplate extends AbstractOAuth2ApiBinding implements VK {
    private IUsersOperations usersOperations;
    private IWallOperations wallOperations;
    private IFriendsOperations friendsOperations;
    private ISecureOperations secureOperations;
    private ILocationOperations locationOperations;
    private IAudioOperations audioOperations;
    private IFeedOperations feedOperations;

    private ObjectMapper objectMapper;

    private final String accessToken;

    protected final String clientSecret;

    // TODO: remove?
    public VKTemplate() {
        initialize();
        this.accessToken = null;
        this.clientSecret = null;
    }

    public VKTemplate(String accessToken, String clientSecret) {
        super(accessToken);
        this.accessToken = accessToken;
        this.clientSecret = clientSecret;
        initialize();
    }

    private void initialize() {
        registerJsonModule();
        getRestTemplate().setErrorHandler(new VKontakteErrorHandler());
        initSubApis();
    }


    @Override
    public IGroupsOperations groupsOperations() {
        return null;
    }

    @Override
    public INewsFeedOperations newsFeedOperations() {
        return null;
    }

    private void registerJsonModule() {
        List<HttpMessageConverter<?>> converters = getRestTemplate().getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter jsonConverter = (MappingJackson2HttpMessageConverter) converter;

                List<MediaType> mTypes = new LinkedList<MediaType>(jsonConverter.getSupportedMediaTypes());
                mTypes.add(new MediaType("text", "javascript", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
                jsonConverter.setSupportedMediaTypes(mTypes);

                objectMapper = new ObjectMapper();
                objectMapper.registerModule(new VKModule());
                jsonConverter.setObjectMapper(objectMapper);
            }
        }
    }

    private void initSubApis() {
        usersOperations = new UsersTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
        friendsOperations = new FriendsTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
        wallOperations = new WallTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
        secureOperations = new SecureTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized(), clientSecret);
        locationOperations = new LocationTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
        audioOperations = new AudioTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
        feedOperations = new FeedTemplate(getRestTemplate(), accessToken, objectMapper, isAuthorized());
    }

    @Override
    public IUsersOperations usersOperations() {
        return usersOperations;
    }

    @Override
    public IWallOperations wallOperations() {
        return wallOperations;
    }

    @Override
    public IFriendsOperations friendsOperations() {
        return friendsOperations;
    }

    @Override
    public ISecureOperations secureOperations() {
        return secureOperations;
    }

    @Override
    public ILocationOperations locationOperations() {
        return locationOperations;
    }

    @Override
    public IAudioOperations audioOperations() {
        return audioOperations;
    }

    @Override
    public IFeedOperations getFeedOperations() {
        return feedOperations;
    }
}
