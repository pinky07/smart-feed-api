package org.springframework.social.vkontakte.api.impl.json.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.vkontakte.api.attachment.Photo;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 14:00
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotosMixin {
    @JsonProperty("count")
    private int count;

    @JsonProperty("items")
    private List<Photo> attachments;
}
