package org.springframework.social.vkontakte.api.impl.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 17:12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupMixin {
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("screen_name")
    private String screenName;
    @JsonProperty("photo_100")
    private String src;
    @JsonProperty("photo_200")
    private String srcBig;
}
