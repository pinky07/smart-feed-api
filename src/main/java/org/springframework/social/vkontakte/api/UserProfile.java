package org.springframework.social.vkontakte.api;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 18:18
 */
public class UserProfile {
    private final static String PROFILE_URL_EXTESTION = "http://vk.com/";
    private final String id;
    private final String firstName;
    private final String lastName;
    private final String screenName;
    private final String gender;
    private final String photo;
    private final String photoSmall;
    private final String profileURL;

    public UserProfile(String id, String firstName, String lastName, String screenName, String gender, String photo, String photoSmall) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.screenName = screenName;
        this.gender = gender;
        this.photo = photo;
        this.photoSmall = photoSmall;
        this.profileURL = PROFILE_URL_EXTESTION + screenName;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getGender() {
        return gender;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotoSmall() {
        return photoSmall;
    }

    public String getProfileURL() {
        return profileURL;
    }
}
