package org.springframework.social.vkontakte.api.impl.json.news;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.social.vkontakte.api.attachment.AttachmentType;
import org.springframework.social.vkontakte.api.news.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 3:59
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(name="post", value=NewsPost.class),
        @JsonSubTypes.Type(name="photo", value=NewsPhoto.class),
        @JsonSubTypes.Type(name="wall_photo", value=NewsWallPhoto.class)
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsItemMixin {
    @JsonProperty("type")
    @JsonDeserialize(using = NewsItemTypeDeserializer.class)
    NewsItemType type;

    @JsonProperty("post_id")
    String id;

    @JsonProperty("date")
    @JsonDeserialize(using = DateDeserializer.class)
    Date createdDate;

    @JsonProperty("source_id")
    String authorId;

    static class NewsItemTypeDeserializer extends JsonDeserializer<NewsItemType> {
        @Override
        public NewsItemType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return NewsItemType.valueOf(jp.getText().toUpperCase());
        }
    }

    static class DateDeserializer extends JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            long dateValue = jp.getLongValue();
            return new Date(dateValue*1000);
        }
    }
}
