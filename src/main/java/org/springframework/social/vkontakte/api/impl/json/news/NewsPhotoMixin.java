package org.springframework.social.vkontakte.api.impl.json.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.vkontakte.api.news.Photos;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 14:03
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsPhotoMixin {
    @JsonProperty("photos")
    Photos photos;
}
