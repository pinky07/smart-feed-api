package org.springframework.social.vkontakte.api;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 17:08
 */
public class Group {
    private String id;
    private String name;
    private String screenName;
    private String src;
    private String srcBig;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getSrc() {
        return src;
    }

    public String getSrcBig() {
        return srcBig;
    }
}
