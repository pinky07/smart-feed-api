package org.springframework.social.vkontakte.api.news;

import org.springframework.social.vkontakte.api.attachment.Photo;
import org.springframework.social.vkontakte.api.attachment.PhotoAttachment;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 13:35
 */
public class Photos {
    private int count;
    private List<Photo> attachments;

    public List<Photo> getAttachments() {
        return attachments;
    }
}
