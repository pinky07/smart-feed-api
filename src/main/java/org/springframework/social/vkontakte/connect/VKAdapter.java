package org.springframework.social.vkontakte.connect;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UserProfileBuilder;
import org.springframework.social.vkontakte.api.VK;
import org.springframework.social.vkontakte.api.VKontakteProfile;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 1:18
 */
public class VKAdapter implements ApiAdapter<VK> {
    @Override
    public boolean test(VK vkontakte) {
        try {
            vkontakte.usersOperations().getUser();
            return true;
        } catch (HttpClientErrorException e) {
            return false;
        }
    }

    @Override
    public void setConnectionValues(VK vkontakte, ConnectionValues values) {
        VKontakteProfile profile = vkontakte.usersOperations().getUser();
        values.setProviderUserId(profile.getUid());
        values.setDisplayName(profile.getFirstName() + " " + profile.getLastName());
        values.setProfileUrl("http://vk.com/id" + profile.getUid());
        values.setImageUrl(profile.getPhoto());
    }

    @Override
    public UserProfile fetchUserProfile(VK vkontakte) {
        VKontakteProfile profile = vkontakte.usersOperations().getUser();
        return new UserProfileBuilder()
                .setUsername(profile.getScreenName())
                .setFirstName(profile.getFirstName())
                .setLastName(profile.getLastName())
                .setName(profile.getFirstName() + " " + profile.getLastName())
                .build();
    }

    @Override
    public void updateStatus(VK vkontakte, String message) {
        // vk api does not allow to perform status.set or wall.post methods for websites,
        // so according to method contract we do nothing here
    }
}