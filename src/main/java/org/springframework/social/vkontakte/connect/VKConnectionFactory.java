package org.springframework.social.vkontakte.connect;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.vkontakte.api.VK;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:32
 */
public class VKConnectionFactory extends OAuth2ConnectionFactory<VK> {
    public VKConnectionFactory(String clientId, String clientSecret) {
        super("vkontakte", new VKServiceProvider(clientId, clientSecret), new VKAdapter());
    }
}
