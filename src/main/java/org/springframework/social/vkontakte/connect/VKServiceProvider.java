package org.springframework.social.vkontakte.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.vkontakte.api.VK;
import org.springframework.social.vkontakte.api.impl.VKTemplate;
import org.springframework.social.vkontakte.api.impl.VKontakteTemplate;
import org.springframework.social.vkontakte.connect.VKontakteOAuth2Template;

/**
 * Author: Khamidullin Kamil
 * Date: 04.11.14
 * Time: 0:18
 */
public class VKServiceProvider extends AbstractOAuth2ServiceProvider<VK> {
    private String clientSecret;

    public VKServiceProvider(String clientId, String clientSecret) {
        super(new VKontakteOAuth2Template(clientId, clientSecret));
        this.clientSecret = clientSecret;
    }

    public VK getApi(String accessToken) {
        return new VKTemplate(accessToken, clientSecret);
    }
}
