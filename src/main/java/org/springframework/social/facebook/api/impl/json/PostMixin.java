//package org.springframework.social.facebook.api.impl.json;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonDeserializer;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import org.springframework.social.facebook.api.*;
//import org.springframework.social.facebook.api.Post;
//
//import java.io.IOException;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
///**
//* Author: Khamidullin Kamil
//* Date: 23.11.14
//* Time: 2:15
//*/
//@JsonIgnoreProperties(ignoreUnknown = true)
//abstract class PostMixin extends FacebookObjectMixin {
//
//    @JsonProperty("id")
//    String id;
//
//    @JsonProperty("actions")
//    List<Action> actions;
//
//    @JsonProperty("application")
//    Reference application;
//
//    @JsonProperty("caption")
//    String caption;
//
//    @JsonProperty("created_time")
//    Date createdTime;
//
//    @JsonProperty("description")
//    String description;
//
//    @JsonProperty("from")
//    Reference from;
//
//    @JsonProperty("icon")
//    String icon;
//
//    @JsonProperty("is_hidden")
//    boolean isHidden;
//
//    @JsonProperty("link")
//    String link;
//
//    @JsonProperty("message")
//    String message;
//
//    @JsonProperty("message_tags")
//    @JsonDeserialize(using=MessageTagMapDeserializer.class)
//    Map<Integer,List<MessageTag>> messageTags;
//
//    @JsonProperty("name")
//    String name;
//
//    @JsonProperty("object_id")
//    String objectId;
//
//    @JsonProperty("picture")
//    String picture;
//
//    @JsonProperty("place")
//    Page place;
//
//    @JsonProperty("privacy")
//    Post.Privacy privacy;
//
//    @JsonProperty("properties")
//    List<PostProperty> properties;
//
//    @JsonProperty("source")
//    String source;
//
//    @JsonProperty("status_type")
//    @JsonDeserialize(using = StatusTypeDeserializer.class)
//    Post.StatusType statusType;
//
//    @JsonProperty("story")
//    String story;
//
//    @JsonProperty("to")
//    @JsonDeserialize(using = ReferenceListDeserializer.class)
//    List<Reference> to;
//
//    @JsonProperty("type")
//    @JsonDeserialize(using = PostTypeDeserializer.class)
//    Post.PostType type;
//
//    @JsonProperty("updated_time")
//    Date updatedTime;
//
//    @JsonProperty("with_tags")
//    @JsonDeserialize(using = ReferenceListDeserializer.class)
//    List<Reference> withTags;
//
//    @JsonProperty("shares")
//    @JsonDeserialize(using = CountDeserializer.class)
//    Integer sharesCount;
//
//    @JsonIgnoreProperties(ignoreUnknown = true)
//    public abstract static class PrivacyMixin {
//
//        @JsonProperty("description")
//        String description;
//
//        @JsonProperty("value")
//        Post.PrivacyType value;
//
//        @JsonProperty("friends")
//        Post.FriendsPrivacyType friends;
//
//        @JsonProperty("networks")
//        String networks;
//
//        @JsonProperty("allow")
//        String allow;
//
//        @JsonProperty("deny")
//        String deny;
//
//    }
//
//    private static class PostTypeDeserializer extends JsonDeserializer<Post.PostType> {
//        @Override
//        public Post.PostType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//            try {
//                return Post.PostType.valueOf(jp.getText().toUpperCase());
//            } catch (IllegalArgumentException e) {
//                return Post.PostType.UNKNOWN;
//            }
//        }
//    }
//
//    private static class StatusTypeDeserializer extends JsonDeserializer<Post.StatusType> {
//        @Override
//        public Post.StatusType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//            return Post.StatusType.valueOf(jp.getText().toUpperCase());
//        }
//    }
//
//    private static class CountDeserializer extends JsonDeserializer<Integer> {
//        @Override
//        public Integer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//            Map map = jp.readValueAs(Map.class);
//            return map.containsKey("count") ? Integer.valueOf(String.valueOf(map.get("count"))): 0;
//        }
//    }
//}
